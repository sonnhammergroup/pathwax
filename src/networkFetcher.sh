#!/bin/bash


#this file builds the DB for PathwAX
#asuming compact FC3 File 
# PFC FBS GeneA GeneB

fctermSetIds="25 26"
#TERM ID 25=metabolic KEGG 26=Signaling

#funcoup="funcoup.sbc.su.se"
#funcoup="192.168.122.97"
funcoup="funcoup.scilifelab.se"
speciesMap="species.map"

#Kegg_name	NCBI_taxonomy	Network_id	Scientific_name
#cel	6239	296241	Caenorhabditis elegans
#cfa	9615	296240	Canis familiaris
#cin	7719	296239	Ciona intestinalis
#dre	7955	296238	Danio rerio
#dme	7227	296237	Drosophila melanogaster
#gga	9031	296236	Gallus gallus
#sce	559292	296235	Saccharomyces cerevisiae S288c
#rno	10116	296234	Rattus norvegicus
#hsa	9606	296230	Homo sapiens
#mmu	10090	296231	Mus musculus
#ath	3702	296242	Arabidopsis thaliana

#fctermSetIds="2 3"
#fcInsatanceID=296205
fctermSetIds="25 26"
fcInsatanceID=7617155
keggMap="http://www.kegg.jp/dbget-bin/www_bget?pathway"

if [ -z $1 ];then
	echo "Specify dbDir"
	exit
fi

if [ -e "$dbDir/$speciesMap" ];then
	echo "Species File Missing"
	exit
fi

dbDir=$1



if [ ! -d "$dbDir/$fcInsatanceID/" ];then
	mkdir "$dbDir/$fcInsatanceID"
fi


fcGenomeIDs=$(cat "$dbDir/$speciesMap" | awk 'NR!=1{print $3}')
#set genome / species /genes /pathways



for fcGenomeID in $fcGenomeIDs;do
#http://funcoup.scilifelab.se/downloads/download.action?type=network&instanceID=7617155&fileName=FC4.0_A.thaliana_compact.gz

echo "[DEBUG]		"
	name="FC4.0_$(cat "$dbDir/$speciesMap" | grep $fcGenomeID| awk '{print $4}'| cut -c1-1).$(cat "$dbDir/$speciesMap" | grep $fcGenomeID | awk '{print $5}')_compact"
	taxID=$(cat "$dbDir/$speciesMap" | grep $fcGenomeID | awk '{print $2}')



echo "[DEBUG]		---------------START-----------------"
echo "[DEBUG]		Processing $name"

	
	file=$(curl -s "http://$funcoup/downloads/download.action?type=network&instanceID=$fcInsatanceID&fileName=$name.gz" | gunzip)

	if [ ! -d "$dbDir/$fcInsatanceID/$fcGenomeID" ];then
		mkdir "$dbDir/$fcInsatanceID/$fcGenomeID"
	fi

	genomeDir="$dbDir/$fcInsatanceID/$fcGenomeID/genes"
	if [ ! -d "$genomeDir" ];then
		mkdir "$genomeDir"
	fi

		

	genome=$(echo "$file" | awk 'NR!=1{print $3}')\n$(echo "$file" | awk 'NR!=1{print $4}')

	genome=$(echo "$genome" | sort | uniq )



echo "[DEBUG]		Requesting FunCoup internal GeneIDs"
	query=$(echo "$genome" | awk '0!=(NR+1)%100{printf $1","};0==(NR+1)%100{print $1" "};END{print $1}')

	for genes in $query;do

		fcgene=$(curl -s "$funcoup/api/geneQuery?taxID=$taxID&query=$genes"| awk 'NR!=1{print $2"\t"$3}')
		echo "$fcgene"  >> "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map"
	done

echo "[DEBUG]		Clean gene.map"
	empty=$(cat "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map" | awk  '{if($1=="")print NR}')
	testify=$(cat "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map" | awk  '{if($1!="")print}' |wc -l)
	orig="$(cat "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map" | wc -l)"

 	if [ ! -z $empty ];then
		tmp="$(cat "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map" | awk  -v e=$empty 'NR!=e{print}')"
		if [ $(echo "$tmp" | wc -l) -eq $testify ];then 
			echo "$tmp" > "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map"
echo "[DEBUG]		$(echo empty | wc -l) blanks found : cleaned $orig -> to $(cat "$dbDir/$fcInsatanceID/$fcGenomeID/gene.map" | wc -l) \t [OK]"
		fi
	fi

echo "[DEBUG]		Saving $name"
	echo "$file" > "$dbDir/$fcInsatanceID/$fcGenomeID/$name"

echo "[DEBUG]		--------------DONE-------------------"
echo "[DEBUG]		"

done




