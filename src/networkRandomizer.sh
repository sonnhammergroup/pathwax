#!/bin/bash

binox='~/Projects/binox/./BinoX'
cutoff=0.8
iterations=1000
speciesMap="species.map"
fcInsatanceID=7617155

for i in `ls "$fcInsatanceID/"`;do

echo "[DEBUG]		---------------$i-----------------"
dir="$(pwd $i)/$fcInsatanceID/$i/"
name="$(ls $dir | grep 'FC4')"
tmpfile=$dir$name"_tmp"

echo "[DEBUG]		Processing $name"

cat $dir$name | awk -F '\t' 'NR>1{print $3"\t"$4"\t"$1}' > $tmpfile

~/Projects/binox/./BinoX -n $tmpfile -i $iterations -c $cutoff > $dir"binox.info"

cp "BinoX-"$name"_tmp.randNet" $dir"BinoX-"$name".randNet"
rm "BinoX-"$name"_tmp.randNet"
rm $tmpfile

echo "[DEBUG]		---------------DONE-----------------"
done




