#!/usr/bin/env python
# by Christoph Ogris
# this script translates whatever you want :)

#structure:
# {gene internal ID : {}}

import os, sys,csv,urllib2,json

def chunker(seq, size):
    return (seq[pos:pos + size] for pos in xrange(0, len(seq), size))

def fc_keggNCBI(species):
    keggReset = {}  
    url = 'http://rest.kegg.jp/conv/'+species+'/ncbi-geneid'
    response = urllib2.urlopen(url)
    cr = csv.reader(response)
    for row in cr:
        (A,B) = row[0].strip().split('\t')
        ncbiID = A.split(':')[1]
        keggID = B.split(':')[1]
        keggReset[ncbiID] = keggID
    
    return keggReset
    
def fc_keggTranslate(kegg,taxID):
    query = {}  
    for genes in chunker(kegg.keys(), 100):
        q = ','.join(genes)
        url = 'http://funcoup.scilifelab.se/api/geneQuery?taxID='+taxID+'&query='+q  
        response = urllib2.urlopen(url)
        cr = csv.reader(response)

        for row in cr:
            if(row[0][0] != '#' and len(row[0].strip().split('\t')) == 3):
                ID={'kegg':[],'ensembl':[],'ncbi':[]} 
                (ncbi, B, C) = row[0].strip().split('\t')
                if (B not in query):
                    query[B] = {}
                    query[B]['PATHWAY'] = {}
                    query[B]['PATHWAY']['kegg']=[]
                    query[B]['ID'] = ID
                    query[B]['ID']['ensembl'] = []
                    query[B]['ID']['ensembl'].append(str(C))
                    query[B]['ID']['kegg'] = []
                    query[B]['ID']['kegg'].append(str(kegg[ncbi]))
                    query[B]['ID']['ncbi'] = []
                    query[B]['ID']['ncbi'].append(str(ncbi))
                else:
                    query[B]['ID']['ensembl'].append(str(C))
                    query[B]['ID']['kegg'].append(str(kegg[ncbi]))
                    query[B]['ID']['ncbi'].append(str(ncbi))
        #break # ONLY FOR DEBUGING				
		
    return query

def readSpeciesMap():
    os.path.isfile('species.map')
    #print 'Dictonary file :\t', dictfile
    f = open('species.map', 'r')
    species = {}
    for line in f:
        if(line[0] != '#'):
            line = line.strip()
            l = line.split('\t')
            species[l[1]] = {}
            species[l[1]]['short'] = l[0]
            species[l[1]]['fcID'] = l[2]
            species[l[1]]['taxid'] = l[1]
    return species
        
def main(argv):
    instanceID= argv[0]
    species = readSpeciesMap()
    for i in species:
        sDir = instanceID+'/'+species[i]['fcID']+'/'
        if(os.path.isdir(sDir)):
            print(species[i]['short'])
            keggGenome = fc_keggNCBI(species[i]['short'])
            genome = fc_keggTranslate(keggGenome,species[i]['taxid'])
            with open(sDir+'genome.json', 'w') as outfile:
                json.dump(genome, outfile)
	
if __name__ == "__main__":
	main(sys.argv[1:])
