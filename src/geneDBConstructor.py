#!/usr/bin/env python
# author: christoph.ogris@scilifelab.se
# required structure and files:
#<database dir>|
#              |<fc instanceID>|
#                              |<fc species ID>|
#                              |<fc species ID>|
#                                              |genes|                                         
#                                              |binox.info
#                                              |BinoX-<species_network>.randNet
#                                              |<fc network file>
#                                              |gene.map 
#                                 
# copy and run this script within <database dir> 
# COMMAND: python geneDBConstructor.py <fc instanceID>

import os
import sys

class IOUtils():
	def __init__(self,file_name):
		self.file_name = file_name
	

	def input_file_iterator(self,sep="\t"):
		try:
			#load lines in chunks into heap memory
			try:
				doc = open(self.file_name,'r')
			except:
				raise IOError("[ERROR] cannot read: " + self.file_name)
			while 1:
				lines = doc.readlines(50)
				#breakpoint of while loop
				if len(lines) == 0:
					break
	
				while (len(lines) > 0):
					line = lines[0].strip().split(sep)			
					
					yield line		
								 
					del lines[0]
			
		except:
			raise


class Gene():
	def __init__(self, ensembl_ID, map_line=None, internal_ID=None):
		self.ensembl_ID = str(ensembl_ID)
		self._internal_ID = internal_ID
		self._map_line = map_line
		self.links_random = []
		self.links_real = []

	
	def init_links(self,size):
		self.links_random = [0] *size
		self.links_real = [0] * size
	
	@property
	def internal_ID(self):
		return self._internal_ID

	@internal_ID.setter
	def internal_ID(self, value):
		self._internal_ID = value

	@property
	def map_line(self):
		return self.map_line

	@map_line.setter
	def map_line(self, value):
		self._map_line = value

	

	def __str__(self):
		return str([str(self.map_line),str(self.internal_ID),str(self.ensembl_ID)])
	
	

class Genome():
	def __init__(self,file_map): 
		self.file_map = file_map
		self.gene_container_dict = {}
		self.size = 0
		
		self.load_file_map()
		
	def load_file_map(self):	
		
		_file = IOUtils(self.file_map)
		_iter = _file.input_file_iterator()
		
		self.size = 0
		for element in _iter:
			self.size = self.size + 1
			self.add_gene(element[0],element[1].upper(),self.size)
			
		for element in self.gene_container_dict:
			self.gene_container_dict[element].init_links(self.size)
			
	
	def add_gene(self,internal_ID,ensembl_ID,line):
		gene = Gene(ensembl_ID)
		gene.internal_ID = internal_ID
		gene.map_line = line
		
		self.gene_container_dict[ensembl_ID] = gene

	def find_gene_by_ensembl_ID(self,ensembl_ID):
		return self.gene_container_dict[ensembl_ID]
	
	def count_links(self):
		count = 0
		for i in self.gene_container_dict:
			gene = self.gene_container_dict[i]
			count = count + sum(gene.links_real)
		
		return count
		
class PreprocessedNetwork():
	def __init__(self,input_file,file_map):
	
		self.genome = Genome(file_map)
		print self.genome.size
		self.dir_dataase = ''
		
		self.input_file = input_file
		self.load_network()
		
		print self.genome.count_links()

	def load_network(self):
		_file = IOUtils(self.input_file)
		_iter = _file.input_file_iterator()	
		
		count = 0
		for element in _iter:
			if(element[0] == "#"):
				print element
			else:
				geneA = self.genome.find_gene_by_ensembl_ID(element[0])
				geneB = self.genome.find_gene_by_ensembl_ID(element[1])
				
				if(geneA == geneB):
					print "connected to itself"
					break
				
				geneA.links_real[geneB.map_line-1]		= int(element[3]) 
				geneA.links_random[geneB.map_line-1]	= int(element[2]) 
				
				geneB.links_real[geneA.map_line-1]		= int(element[3]) 
				geneB.links_random[geneA.map_line-1]	= int(element[2]) 
		
				if(int(element[3]) != 0):
					count = count + 1


class DataBase():
	def __init__(self,genome):
		self.dir_database = ''
		self.genome = genome
		
	def write(self, dir_output):
		self.dir_database = dir_output
		
		if not os.path.exists(dir_output):
			os.makedirs(dir_output)
		
		
		for i in self.genome.gene_container_dict:
			gene = self.genome.gene_container_dict[i]
			random = gene.links_random
			real = gene.links_real
			
			if(sum(real) == 0):
				continue
			
			_file_name = dir_output + gene.internal_ID
			output = open(_file_name, 'w')
			
			
			for j in range(0,self.genome.size-1):
				output.write(str(random[j]))
				output.write("\t")
				output.write(str(real[j]))
				output.write("\n")
				
			output.close()
   
		
if __name__ == '__main__':
	
	#genome = Genome('cin','DB/gene.map')
	#print genome.find_gene_by_ensembl_ID('ENSCING00000000248')
	species = [x[1] for x in os.walk(sys.argv[1])][0]
	
	for i in species:
	
	    print '-------'+i+'--------'
	    print 'read network'
	    
	    for file in os.listdir(sys.argv[1]+'/'+i):
	        if file.endswith(".randNet"):
	            binoxfile = file
	    
	    print i+'/'+binoxfile
	    network = PreprocessedNetwork(sys.argv[1]+'/'+i+'/'+binoxfile,sys.argv[1]+'/'+i+'/gene.map')
	
	    print 'write DataBase'
	    print i+'/genes/'
	    database = DataBase(network.genome)
	    database.write(sys.argv[1]+'/'+i+'/genes/')
	
	
	    print ' DONE'
	    #print sys.argv[1:]
        
 
