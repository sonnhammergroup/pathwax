#!/usr/bin/env python
# by Christoph Ogris

#structure:
# {gene internal ID : {}}

import os, sys, getopt,json,csv, urllib2
from os import listdir
from os.path import isfile, join

def readSpeciesMap(filename):
    os.path.isfile(filename)
    #print 'Dictonary file :    ', dictfile
    f = open(filename, 'r')
    species = {}
    for line in f:
        if(line[0] != '#'):
            line = line.strip()
            l = line.split('\t')
            species[l[1]] = {}
            species[l[1]]['short'] = l[0]
            species[l[1]]['long'] = l[3]
            species[l[1]]['fcID'] = l[2]
            species[l[1]]['taxid'] = l[1]
    return species

def readPathwayList(speciespl):
    url = 'http://rest.kegg.jp/list/pathway/'+speciespl
    response = urllib2.urlopen(url)
    cr = csv.reader(response)
    result={}
    for row in cr:
        (keggID, Name) = row[0].strip().split('\t')    
        
        #cleaning name
        tmp = Name.split(' - ')
        removed = tmp[len(tmp)-1]
        Name = " ".join(tmp[:len(tmp)-1])
        result[keggID] = Name
    print 'Pathwayname cleaned by removing: '+removed
    
    return result

def fetchPathway(keggID,keggName,tmp_genome,speciesfp):
    url = 'http://rest.kegg.jp/get/'+keggID
    response = urllib2.urlopen(url)
    cr = csv.reader(response)
    result={}
    result['GENE'] = []
    result['NAME'] = []
    result['CLASS'] = {}
    result['PATHWAY_MAP'] = []
    result['ORGANISM'] = []
    result['DESCRIPTION'] = []
    result['FC4'] = 0
    
    method = '' 
    for l in cr:
        line=l[0]
        A = line.split(' ')
        if(A[0] is not ''):
	        method = A[0]	
        
        if(method == 'GENE'):
            try:
                if(len(result['GENE']) == 0):
                    gene = A[8]
                    if(A[0]!='GENE'):
                        gene = A[12]
                        
                    if(gene in tmp_genome.keys()):
                        result['GENE'].append(tmp_genome[gene]['kegg'].encode( 'utf-8' ))
                        result['FC4'] = result['FC4'] + tmp_genome[gene]['links']
                elif(any(A[12].strip())):
                    gene = A[12]
                    if(gene in tmp_genome.keys()):
                        result['FC4'] = result['FC4'] + tmp_genome[gene]['links']
                        result['GENE'].append(tmp_genome[gene]['kegg'].encode( 'utf-8' ))
            except:
                    print 'error : '
                    print A
            #return 0
	
        if(method == 'NAME'):
	        result['NAME'].append(keggName)


        if(method == 'CLASS'):
	        part = ' '.join(A[7:]).strip() 
	        if(any(part)):
		        (cl,subcl) = part.split(';')
		        result['CLASS'][cl] = subcl
	
	
        if(method == 'ORGANISM'):
	        result['ORGANISM'].append(speciesfp['long'])

        if(method == 'DESCRIPTION'):
	        result['DESCRIPTION'].append(' '.join(A[1:]).strip())

        if(method == 'PATHWAY_MAP'):
	        result['PATHWAY_MAP'].append(A[1])
    return result

def loadNetwork(netDir,cuttoff):
	
	for file in os.listdir(netDir):
                if file.endswith("_compact"):
                    fcFile = file

	
	networkFile =  netDir+fcFile

	os.path.isfile(networkFile)
	f = open(networkFile, 'r')
	network_tmp = {}

        cnt = 0
        for line in f:
                if(cnt != 0):
                        line = line.strip()
                        (A, B, C, D) = line.split('\t')
                        if (float(A) >= cuttoff):
                                if(C not in network_tmp):
                                        network_tmp[C] = 1

                                if(C in network_tmp):
                                        network_tmp[C] = network_tmp[C] + 1

                                if(D not in network_tmp):
                                        network_tmp[D] = 1

                                if(D in network_tmp):
                                        network_tmp[D] = network_tmp[D] + 1
                cnt = cnt + 1
	return network_tmp 

def main(argv):
    instanceID= argv[0]
    species = readSpeciesMap('species.map');
				    
    for i in species.keys():
	network=loadNetwork(instanceID+'/'+species[i]['fcID']+'/',0.8)
        with open(instanceID+'/'+species[i]['fcID']+'/genome.json') as data_file:    
	    genome = json.load(data_file)
    	    
        tmp_genome = {}    
        for g in genome.keys():
            if (any(genome[g]['ID']['kegg'])):
                for m in genome[g]['ID']['kegg']:
                    if(m not in tmp_genome.keys()):
                        tmp_genome[m] = {}
                        tmp_genome[m]['kegg'] = g
                        tmp_node = unicode(genome[g]['ID']['ensembl'][0])
                        if(tmp_node in network.keys()):
                            tmp_genome[m]['links'] = network[unicode(genome[g]['ID']['ensembl'][0])]
                        else:
                            tmp_genome[m]['links'] = 0	
        tmp = {} 
	#get kegg pathway list  
        pathwayList = readPathwayList(species[i]['short'])
        for pathway in pathwayList.keys():
            tmp[pathway] = fetchPathway(pathway,pathwayList[pathway],tmp_genome,species[i])
    #	    return 0
	    
    	result = {}
    	for j in tmp:
		if (not any(tmp[j]['CLASS'])):
			tmp[j]['CLASS']['Other']='Undefined'
		if (any(tmp[j]['GENE'])):
			result[j]=tmp[j]
		else:
			tmp[j]['GENE']

        try:
            os.stat(instanceID+'/'+species[i]['fcID']+'/pathway/')
        except:
            os.mkdir(instanceID+'/'+species[i]['fcID']+'/pathway/') 
	
    	#print result
    	with open(instanceID+'/'+species[i]['fcID']+'/pathway/kegg.json', 'w') as outfile:
        	json.dump(result, outfile)    

if __name__ == "__main__":
	main(sys.argv[1:])		

