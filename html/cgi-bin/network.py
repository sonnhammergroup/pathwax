#!/usr/bin/env python
# author: christoph.ogris@scilifelab.se

import database

class Network():
    def __init__(self,instance_database, dir_database, species_name):
        
        self.database_network = database.Network(
                                instance_database, 
                                dir_database, 
                                species_name    )
        
        self.genome_all = self.database_network.genome.gene_list()
        
        
        
    def get_adjacent(self,query):  
      #get query couplings
        _query_couplings_idx    =    {}
        _query_couplings_idx[    'boundNode'    ]    =    []
        _query_couplings_idx[    'freeNode'    ]    =    []
        _query_couplings_idx[    'adjacent'    ]    =    []    
        
        _query = query
        #iterate over genes in query and find their property file
        for gene in _query:
            _coup_idx = self.database_network.genes_adjacent(gene)
               
            if(_coup_idx != None):
                _coup_idx = [i for i in _coup_idx]    #get index of functional associated genes
                _query_couplings_idx[    'adjacent'    ]    +=    _coup_idx            #functional association counter
                _query_couplings_idx[    'boundNode'    ].append(gene)
            else:
                _query_couplings_idx[    'freeNode'    ].append(gene)
                
           #sort list and return uniqe list of functional associated genes
        _query_couplings_idx['adjacent'] = sorted(list(set(_query_couplings_idx['adjacent'])))
        _couplings = [self.genome_all[i] for i in _query_couplings_idx['adjacent']] 
        _query_couplings_idx['adjacent'] = _couplings
        
        return _query_couplings_idx         
                
                
    def get_links(self,query,adjacent):
        #prepare result dict
        _result = {}
        #_result['random'] = []
        _result['real'] = []
        
        #prepare queryIDX
        _query_idx = [self.genome_all.index(i) for i in query if i in self.genome_all]
        _query_idx_not_sortet = _query_idx
        _query_idx = sorted(list(set(_query_idx)))
        
        for gene in adjacent:
            #_tmp_random_links = [0]*len(_query_idx)
            _tmp_funcoup_links  = [0]*len(_query_idx)
            _idx = 0
            
            genes_idx = self.database_network.gene_links(gene, _query_idx)
#            print gene
            #print _query_idx
            for entry in genes_idx:
                row = entry.strip().split('\t')
                if len(row)<2:
                    raise TypeError("[ERROR] gene data corrupt")    

                #_tmp_random_links[_query_idx_not_sortet.index(_query_idx[_idx])] = int(row[0])    
                _tmp_funcoup_links[_query_idx_not_sortet.index(_query_idx[_idx])]  = int(row[1])
                
                _idx = _idx +1
    
            #_result['random' ].append(_tmp_random_links)
            _result['real' ].append(_tmp_funcoup_links)           

        return _result
        
    def get_pathway_graph(self,query,pathway):
        #prepare result dict
        _result = {}
        _nodes  = []
        _links  = []
        
        #return query
        allQuery = list(set(query).union(set(pathway)))
        
        _fcID = [i for i in allQuery if i in self.genome_all]
        
        _query_idx = [self.genome_all.index(i) for i in allQuery if i in self.genome_all]
        _query_idx_not_sortet = _query_idx
        _query_idx = sorted(list(set(_query_idx)))
        
        _nodes = {}
        for i in _query_idx:
            _nodes[i] = _fcID[_query_idx_not_sortet.index(i)]
        
        
        _tmp = []
        for gene in _nodes:
            #_tmp_random_links = [0]*len(_query_idx)
            _tmp_funcoup_links  = [0]*len(_query_idx)
            _idx = 0
            
            genes_idx = self.database_network.gene_links(_nodes[gene], _query_idx)
 
#            print gene
            #print _query_idx
            for entry in genes_idx:
                row = entry.strip().split('\t')
                if len(row)<2:
                    raise TypeError("[ERROR] gene data corrupt")    
                                
                if(int(row[1]) > 0):
                    #return _nodes[_query_idx[_idx]]
                    _geneA = _nodes[_query_idx[_idx]]
                    _geneB = _nodes[gene]
                    if((str(_geneB) + str(_geneA)) not in _tmp ):
                        if((str(_geneA) + str(_geneB)) not in _tmp ):
                            _link = { "source": _geneB, "target": _geneA, "value": int(row[1]), "random":int(row[0]) }
                            _links.append(_link) 
                            _tmp.append(str(_geneA) + str(_geneB))
                
                    
                _idx = _idx +1

        _nodesclean = []
        for gene in _fcID:
            if(gene in pathway and gene in query):
                _nodesclean.append({"id":gene,"group":"overlap"})
            elif(gene in query):
                _nodesclean.append({"id":gene,"group":"signature"})
            elif(gene in pathway):
                _nodesclean.append({"id":gene,"group":"pathway"})
            else:
                raise TypeError("[ERROR] gene data corrupt")   
                        
        r = {"nodes":_nodesclean, "links":_links}       
        return r 
         
    def sum_random_links(self,query,adjacent):
        #prepare result dict
        _result = []
        
        #prepare queryIDX
        _query_idx = [self.genome_all.index(i) for i in query if i in self.genome_all]
        _query_idx_not_sortet = _query_idx
        _query_idx = sorted(list(set(_query_idx)))
        
        for gene in adjacent:
            _tmp_random_links = [0]*len(_query_idx)
            _idx = 0
            
            
            if self.database_network.valid_gene(gene):    
                genes_idx = self.database_network.gene_links(gene, _query_idx)
    #            print gene
                #print _query_idx
                for entry in genes_idx:
                    row = entry.strip().split('\t')
                    if len(row)<2:
                        raise TypeError("[ERROR] gene data corrupt")    
    
                    _tmp_random_links[_query_idx_not_sortet.index(_query_idx[_idx])] = int(row[0])   
                    
                    _idx = _idx +1
        
                _result = _result + _tmp_random_links
       

        return sum(_result)
        
        
        
        
                       
      
    