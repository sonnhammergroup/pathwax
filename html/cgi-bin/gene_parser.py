#!/usr/bin/env python
import urllib2
import database

class EnsembleIDParser():
    def __init__(self, instance_database, dir_database, species_name):
        """   
        INPUT:
            1) species ID
            2) db instance
            3) db dir
        """
        self.genome = database.Genome(instance_database, dir_database, species_name)
        
        self.query = []
        self.result = {}
        
        
    def fetch(self,query):
        """
        INPUT:
            1)internal query genes
            [192995, 193017, 193034]
        
        OUTPUT:
            {0: {'internalGeneID': 192995, 
                'geneID': 'ENSCING00000000016'}, 
            1: {'internalGeneID': 193017,
                'geneID': 'ENSCING00000000042'}, 
            2: {'internalGeneID': 193034, 
                'geneID': 'ENSCING00000000255'}
            }
        """
        self.query = query
        self.genome_iterator = self.genome.iterator()
        _idx = 0
        
        for gene in self.genome_iterator:
            if(int(gene[0]) in self.query):     
                _tmp = {}
                _tmp['geneID'] = gene[1].rstrip()
                _tmp['internalGeneID'] = int(gene[0])
                self.result[_idx] = _tmp
                _idx = _idx + 1
        
        return self.result

class FuncoupIDParser():
    def __init__(self, instance_database, dir_database, species, funcoup_URL):
        """   
        INPUT:
            1) TaxID species
            2) funcoup url
            3) funcoup instance used
        """
        self.instance_database = instance_database
        self.dir_database = dir_database
        self.species = database.Species(self.instance_database,
                                        self.dir_database)
        
        self.fetched_species = self.species.fetch_species(species)
        
        self.species = self.fetched_species['taxID']
        self.funcoup_URL = funcoup_URL
        self.api = "/api/geneQuery?"
    
        self.query = []
        self.header = []
        self.result = {}

    def fetch_funcoup_internal_ID(self, query):    
        """
        INPUT: list of genes 
                ENSCING00000000016,ENSCING00000000042,ENSCING00000000255
        OUTPUT: json structure  
            example:
                {0:
                    {'Keywords': 'ENSCING00000000016',
                     'internalGeneID': '192995',
                     'geneID': 'ENSCING00000000016'}, 
                1:
                    {'Keywords': 'FAKE',
                     'internalGeneID': '',
                      'geneID': ''
                      }
                }
         """  
        self.query = query
        
        try:
            _request = urllib2.urlopen(self.funcoup_URL + self.api + "taxID=" + self.species + "&query=" + self.query)
        
            self.header = []
            self.result = {}
            
            _idx = 0
       
        # iterate over species
            for line in _request.readlines():     
                # read header
                if (line[0][0] == "#") & (not bool(self.header)):
                    all_columns = line.split('\t')
                    for column in all_columns:
                        self.header.append(column.replace("#", "").rstrip())
                        
                if line[0][0] != "#":
                    if(len(self.header) == 0):
                        raise TypeError("please include header to" % self.file)
                    
                    tmp = {}
                    all_columns = line.split('\t')
                    for column in all_columns:
                            tmp[self.header[all_columns.index(column)]] = column.rstrip()
                    
                    self.result[_idx] = tmp
                    _idx = _idx + 1        
            return self.result
            
        except urllib2.URLError, e:
            raise TypeError("[ERROR] cannot establish connection to FunCoup")          
    
        return None

    def fetch_funcoup_alternative_ID(self, query, alternative):    
        """
        INPUT: list of genes 
                ENSCING00000000016,ENSCING00000000042,ENSCING00000000255
               alternative ID
                Symbol
        OUTPUT: json structure  
            example:
                {0:
                    {'Keywords': 'ENSCING00000000016',
                     'internalGeneID': '192995',
                     'geneID': 'ENSCING00000000016'}, 
                1:
                    {'Keywords': 'FAKE',
                     'internalGeneID': '',
                      'geneID': ''
                      }
                }
         """  
        self.query = query
        self.alternative = alternative
        
        try:

       
            _request = urllib2.urlopen(self.funcoup_URL + self.api + "taxID=" + self.species + "&query=" + self.query + "&identifierSource=" + self.alternative)
        
            self.header = []
            self.result = {}
            
            _idx = 0
       
        # iterate over species
            for line in _request.readlines():     
                # read header
                if (line[0][0] == "#") & (not bool(self.header)):
                    
                    all_columns = line.split('\t')
                    for column in all_columns:
                        self.header.append(column.replace("#", "").rstrip())
                        
                if line[0][0] != "#":
                    if(len(self.header) == 0):
                        raise TypeError("please include header to" % self.file)
                    
                    tmp = {}
                    all_columns = line.split('\t')
                    for column in all_columns:   
                            tmp[self.header[all_columns.index(column)]] = column.rstrip()
                    
                    self.result[_idx] = tmp
                    _idx = _idx + 1
            return self.result
            
        except urllib2.URLError, e:
            raise TypeError("[ERROR] cannot establish connection to FunCoup")          
    
        return None
        
    def fetch_funcoup_internal_ID_offline(self, query):
        """
        for offline development only 
        """
        return {"0": {"Keywords": "ENSCING00000000268", "internalGeneID": "190121", "geneID": "ENSCING00000000268"},
                 "1": {"Keywords": "ENSCING00000000064", "internalGeneID": "190132", "geneID": "ENSCING00000000064"},
                 "2": {"Keywords": "ENSCING00000000068", "internalGeneID": "190135", "geneID": "ENSCING00000000068"},
                 "3": {"Keywords": "ENSCING00000000170", "internalGeneID": "190439", "geneID": "ENSCING00000001229"},
                 "4": {"Keywords": "ENSCING00000000163", "internalGeneID": "190566", "geneID": "ENSCING00000000163"},
                 "5": {"Keywords": "ENSCING00000000160", "internalGeneID": "190568", "geneID": "ENSCING00000000160"}, 
                 "6": {"Keywords": "ENSCING00000000177", "internalGeneID": "190597", "geneID": "ENSCING00000000177"}, 
                 "7": {"Keywords": "ENSCING00000000170", "internalGeneID": "190608", "geneID": "ENSCING00000000170"}, 
                 "8": {"Keywords": "ENSCING00000000107", "internalGeneID": "190775", "geneID": "ENSCING00000000107"}, 
                 "9": {"Keywords": "ENSCING00000000088", "internalGeneID": "190933", "geneID": "ENSCING00000000088"}, 
                 "10": {"Keywords": "ENSCING00000000010", "internalGeneID": "191074", "geneID": "ENSCING00000000010"}, 
                 "11": {"Keywords": "ENSCING00000000033", "internalGeneID": "191085", "geneID": "ENSCING00000000033"}, 
                 "12": {"Keywords": "ENSCING00000000252", "internalGeneID": "191101", "geneID": "ENSCING00000000252"}, 
                 "13": {"Keywords": "ENSCING00000000212", "internalGeneID": "191172", "geneID": "ENSCING00000000212"}, 
                 "14": {"Keywords": "ENSCING00000000208", "internalGeneID": "191180", "geneID": "ENSCING00000000208"}, 
                 "15": {"Keywords": "ENSCING00000000229", "internalGeneID": "191260", "geneID": "ENSCING00000000229"}, 
                 "16": {"Keywords": "ENSCING00000000226", "internalGeneID": "191287", "geneID": "ENSCING00000000226"}, 
                 "17": {"Keywords": "ENSCING00000000223", "internalGeneID": "191288", "geneID": "ENSCING00000000223"}, 
                 "18": {"Keywords": "ENSCING00000000199", "internalGeneID": "191557", "geneID": "ENSCING00000000199"}, 
                 "19": {"Keywords": "ENSCING00000000162", "internalGeneID": "191578", "geneID": "ENSCING00000000162"}, 
                 "20": {"Keywords": "ENSCING00000000305", "internalGeneID": "191833", "geneID": "ENSCING00000000305"}, 
                 "21": {"Keywords": "ENSCING00000000306", "internalGeneID": "191837", "geneID": "ENSCING00000000306"},
                 "22": {"Keywords": "ENSCING00000000097", "internalGeneID": "191941", "geneID": "ENSCING00000000097"}, 
                 "23": {"Keywords": "ENSCING00000000096", "internalGeneID": "191942", "geneID": "ENSCING00000000096"}, 
                 "24": {"Keywords": "ENSCING00000000029", "internalGeneID": "192026", "geneID": "ENSCING00000000029"}, 
                 "25": {"Keywords": "ENSCING00000000036", "internalGeneID": "192034", "geneID": "ENSCING00000000036"}, 
                 "26": {"Keywords": "ENSCING00000000035", "internalGeneID": "192035", "geneID": "ENSCING00000000035"}, 
                 "27": {"Keywords": "ENSCING00000000034", "internalGeneID": "192036", "geneID": "ENSCING00000000034"}, 
                 "28": {"Keywords": "ENSCING00000000202", "internalGeneID": "192221", "geneID": "ENSCING00000000202"}, 
                 "29": {"Keywords": "FAKE", "internalGeneID": "", "geneID": ""}}

