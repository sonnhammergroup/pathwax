#!/usr/bin/env python

#	author: 	ogris.christoph@scilifelab.se
#	execute:	nosetests -v	
#	conf.ini required

import unittest
import ConfigParser 
import os
import json 
import urllib2

import database
import gene_parser
import network
import crosstalkb

#since configparser cannot read dict directly StringToDict will parse a string into a dict
def string_to_dict(s):
	json_acceptable_string		=	s.replace("'", "\"")
	return json.loads(json_acceptable_string)


class ConfigTestCase(unittest.TestCase):
	def test_set_up(self):
		self.config = ConfigParser.ConfigParser()
		if('wwwconf' in os.environ):
			self.config.read(os.environ['wwwconf'] + "/conf.ini")
		else:
			self.config.read("conf.ini")
		
		 
		if('wwwdata' in os.environ):	   
			self.dir_database = os.environ['wwwdata']
		else:   
			self.dir_database = self.config.get("DataBase","dir")

		#self.WebServerURL = config.get("WebServer",	"url").replace("'", "")


class DataBaseTestCase(unittest.TestCase):
	def config_load(self):
		self.config = ConfigParser.ConfigParser()
		
		if('wwwconf' in os.environ):
			self.config.read(os.environ['wwwconf'] + "/conf.ini")
		else:
			self.config.read("conf.ini")
 
		if('wwwdata' in os.environ):	   
			self.database_dir = os.environ['wwwdata']
		else:   
			self.database_dir = self.config.get("DataBase","dir")
			
			
		self.database_instance = self.config.get("DataBase", "instance")
		
		self.species = database.Species(
										self.database_instance,
										self.database_dir)
		self.test_case = self.species.all_species()
		
		
	def test_species(self):	
		self.config_load()
		
		self.test_short_species_name	= self.config.get("Test", "ShortSpeciesName")
		self.test_long_species_name	= self.config.get("Test", "LongSpeciesName")
		self.test_species_fc_ID = self.config.get("Test", "InternalSpeciesID")
		
		self.assertIsInstance(self.test_case , dict )
		print self.test_case
		self.assertTrue( len(self.test_case) > 0 )
		
		self.short = [self.test_case[i]['short'] for i in self.test_case]
		self.assertTrue(self.test_short_species_name in str(self.short))
		
		self.long = [self.test_case[i]['long'] for i in self.test_case]
		self.assertTrue(self.test_long_species_name in str(self.long))
	
		self.internal_ID = [self.test_case[i]['fcID'] for i in self.test_case]
		self.assertTrue(self.test_species_fc_ID in str(self.internal_ID))
		
		
	def test_fetch_species(self):	
		self.config_load()
		self.test_short_species_name	= self.config.get("Test", "ShortSpeciesName")
		self.test_long_species_name	= self.config.get("Test", "LongSpeciesName")
		self.test_species_fc_ID = self.config.get("Test", "InternalSpeciesID")
		
		
		self.fetched_species = self.species.fetch_species(self.test_short_species_name)
		self.assertTrue(self.fetched_species['short']==self.test_short_species_name)
		
		self.fetched_species = self.species.fetch_species(self.test_species_fc_ID)
		self.assertTrue(self.fetched_species['long']==self.test_long_species_name)
		
	def test_species_dir(self):	
		self.config_load()
		self.test_short_species_name	= self.config.get("Test", "ShortSpeciesName")
		
		self.species_dir = self.species.get_dir_species(self.test_short_species_name)
		self.assertTrue( os.path.isdir(self.species_dir))
		
	def test_genome_random_genes(self):	
		self.config_load()
		self.test_short_species_name	= self.config.get("Test", "ShortSpeciesName")
		
		self.genome = database.Genome(
										self.database_instance, 
										self.database_dir, 
										self.test_short_species_name	)
		
		self.genome_random_genes = self.genome.random_genes(15)
		print self.genome_random_genes
		self.assertTrue( len(self.genome_random_genes.split(',')) == 15)
	
	def test_genome_read_whole(self):
		self.config_load()
		self.test_short_species_name	= self.config.get("Test", "ShortSpeciesName")
		self.test_genome_size = int(		self.config.get("Test", "GenomeSize"))
		
		self.genome = database.Genome(
										self.database_instance, 
										self.database_dir, 
										self.test_short_species_name	)
		
		self.genome_all = self.genome.gene_list()
		print (len(self.genome_all))
		self.assertTrue( len(self.genome_all) == self.test_genome_size)
		

class GeneParserTestCase(unittest.TestCase):
	def config_load(self):
		self.config = ConfigParser.ConfigParser()
		
		if('wwwconf' in os.environ):
			self.config.read(os.environ['wwwconf'] + "/conf.ini")
		else:
			self.config.read("conf.ini")
 
		if('wwwdata' in os.environ):	   
			self.database_dir = os.environ['wwwdata']
		else:   
			self.database_dir = self.config.get("DataBase","dir")
			
		
		self.database_instance = self.config.get("DataBase", "instance")		
		self.test_taxonomy_ID	= self.config.get("Test", "test_taxonomy_ID")
		self.gene_query = self.config.get("Test", "GeneQuery")
		self.test_gene_query_fc_ID = string_to_dict(	self.config.get("Test", "GeneQueryFCID"))
		
	def test_any_to_funcoup(self):
		self.config_load()
		self.funcoup_URL = self.config.get("FunCoup", "url")
		
		
		self.funcoup_internal = gene_parser.FuncoupIDParser(self.database_instance, self.database_dir, self.test_taxonomy_ID, self.funcoup_URL)
		
		self.result_to_funcoup = self.funcoup_internal.fetch_funcoup_internal_ID(self.gene_query)
		self.result_to_funcoup  = [int(self.result_to_funcoup [i]['internalGeneID']) for i in self.result_to_funcoup ]
		self.assertTrue( self.result_to_funcoup  == self.test_gene_query_fc_ID )
		 
	def test_internal_to_ensembl(self):
		self.config_load()
		
		self.internal_to_enseble = gene_parser.EnsembleIDParser(
																self.database_instance,
																self.database_dir,
																self.test_taxonomy_ID)
		self.result_to_ensembl = self.internal_to_enseble.fetch(self.test_gene_query_fc_ID)
		print [self.result_to_ensembl[i]['geneID'] for i in self.result_to_ensembl] 
		print self.gene_query.split(",")
		self.assertTrue( sorted([self.result_to_ensembl[i]['geneID'] for i in self.result_to_ensembl])  == sorted(self.gene_query.split(",")))
		
class NetworkTest(unittest.TestCase):	
	
	def config_load(self):
		self.config = ConfigParser.ConfigParser()
		
		if('wwwconf' in os.environ):
			self.config.read(os.environ['wwwconf'] + "/conf.ini")
		else:
			self.config.read("conf.ini")
 
		if('wwwdata' in os.environ):	   
			self.database_dir = os.environ['wwwdata']
		else:   
			self.database_dir = self.config.get("DataBase","dir")
			
		
		self.database_instance = self.config.get("DataBase", "instance")		
		self.test_taxonomy_ID	= self.config.get("Test", "test_taxonomy_ID")
		self.test_gene_query_fc_ID = string_to_dict(	self.config.get("Test", "GeneQueryFCID"))
			
		self.net = network.Network(
									self.database_instance,
									self.database_dir,
									self.test_taxonomy_ID)
		
	def test_neighbours(self):
		self.config_load()
		self.gene_neighbour	= string_to_dict(	self.config.get("Test", "GeneNeighbour"))
		self.neighbours = self.net.get_adjacent(self.test_gene_query_fc_ID)
		print self.neighbours
		self.assertTrue( self.neighbours == self.gene_neighbour)
		
	
	def test_links(self):
			
		self.config_load()
		self.test_case_links_real = string_to_dict(self.config.get("Test", "links_real"))
		self.test_case_links_random = string_to_dict(self.config.get("Test", "links_random"))
		
		self.internal_to_enseble = gene_parser.EnsembleIDParser(
																self.database_instance,
																self.database_dir,
																self.test_taxonomy_ID)
		
		self.result_to_ensembl = self.internal_to_enseble.fetch(self.test_gene_query_fc_ID)
		
		self.gene_neighbour	= string_to_dict(	self.config.get("Test", "GeneNeighbour"))
		_result = self.net.get_links(self.gene_neighbour['boundNode'],self.gene_neighbour['adjacent'])
		print "Result: " + str(_result['real'])
		print "TestCase: " + str(self.test_case_links_real)
		
		#print "random Result: " + str(_result['random'])
		#print "random TestCase: " + str(self.test_case_links_random)
		self.assertTrue(_result['real'] == self.test_case_links_real)
		#self.assertTrue(_result['random'] == self.test_case_links_random)
		
class PathwayTest(unittest.TestCase):	
	
	def config_load(self):
		self.config = ConfigParser.ConfigParser()
		
		if('wwwconf' in os.environ):
			self.config.read(os.environ['wwwconf'] + "/conf.ini")
		else:
			self.config.read("conf.ini")
 
		if('wwwdata' in os.environ):	   
			self.database_dir = os.environ['wwwdata']
		else:   
			self.database_dir = self.config.get("DataBase","dir")
			
		
		self.database_instance = self.config.get("DataBase", "instance")		
		self.test_taxonomy_ID	= self.config.get("Test", "test_taxonomy_ID")
		self.test_gene_query_fc_ID = string_to_dict(	self.config.get("Test", "GeneQueryFCID"))
		self.test_gene_query_fc_ID = self.config.get("Test", "PathwayTerm")
		
		self.pathways = database.Pathways(
									self.database_instance,
									self.database_dir,
									self.test_taxonomy_ID)
	
	def test_kegg_pathway(self):
		self.config_load()
		self.test_gene_neighbour	= string_to_dict(	self.config.get("Test", "GeneNeighbour"))
		self.test_pathway	= self.config.get("Test", "Pathway")
		
		_result = self.pathways.search(self.test_gene_neighbour['adjacent'],self.test_gene_neighbour['boundNode'],['kegg'])
		
		#print self.test_gene_neighbour['adjacent']	
		print _result['kegg'].keys()[0]
		print _result['kegg']
		
		#self.assertTrue(False)
		self.assertTrue(_result['kegg'].keys()[0] == self.test_pathway)
		
"""		
class CrossTalkBTest(unittest.TestCase):	
	
	def config_load(self):
		self.config = ConfigParser.ConfigParser()
		self.config.read(os.path.abspath("conf.ini"))
		
		self.database_dir = self.config.get("DataBase", "dir")
		self.database_instance = self.config.get("DataBase", "instance")		
		self.test_taxonomy_ID	= self.config.get("Test", "test_taxonomy_ID")
		self.gene_query = self.config.get("Test", "GeneQuery")

	def test_crosstalkb(self):
		self.config_load()
		
		self.CrossTalkB = crosstalkb.CrossTalkB(self.database_instance, self.database_dir, self.test_taxonomy_ID)
		
		print self.CrossTalkB.calculate_crosstalk(self.gene_query)
		#self.assertTrue(False)
"""
#You could execute the test using the standard unittest runner
if __name__ == '__main__':
	unittest.main()
        
