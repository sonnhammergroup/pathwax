#!/usr/bin/env python
# author: christoph.ogris@scilifelab.se
"""
TODO: STILL NEEDS TO BE TESTED PROPERLY!!!
"""

import database
import network
import gene_parser
import ConfigParser 
import os

from scipy import stats
    
class CrossTalkB():
    #query must be in FCinternal ID
    def __init__(self,instance_database, dir_database, species_name):
        
        self.instance_database  = instance_database
        self.dir_database = dir_database
        self.species_name = species_name
        
        
        _config = ConfigParser.ConfigParser()
        _config.read(os.path.abspath("conf.ini"))
        self.funcoup_URL = _config.get("FunCoup", "url")    
        
        
        self.species = database.Species(self.instance_database,
                               self.dir_database)  
        self.species_dir = self.species.get_dir_species(self.species_name)
        self.species_info = self.species.fetch_species(self.species_name)
        
        
        self.parser = gene_parser.FuncoupIDParser(self.species_info['taxID'], self.funcoup_URL)
        
        
        self.net = network.Network(
                                    self.instance_database,
                                    self.dir_database,
                                    self.species_name)      
        
        self.genome = database.Genome(self.instance_database, 
                                self.dir_database, 
                                self.species_name    )
         
        self.pathways = database.Pathways(
                                    self.instance_database,
                                    self.dir_database,
                                    self.species_name)       
        
        
    def calculate_crosstalk(self, query_genes):
        
        self.query_genes = query_genes
        #print query
        #translate

        self.funcoup_internal = self.parser.fetch_funcoup_internal_ID(self.query_genes)
        
        _genes = [ self.funcoup_internal[i]['internalGeneID'] for i in self.funcoup_internal  if self.funcoup_internal[i]['internalGeneID'] != '' ]
        
        
        if(len(_genes)==0):
            return None
        
        _neighbours =  self.net.get_adjacent(_genes)
        _links = self.net.get_links(map(int,_neighbours['boundNode']),map(int,_neighbours['adjacent']))         
        _pathways = self.pathways.search( _neighbours['adjacent'],['kegg'])
        
        _out_degree = sum([sum(i) for i in _links['real']])
        _query_len = len(_neighbours['boundNode'])

      
        _terms  = [i for i in _pathways]
        
        _result = {}
     
        for i in _terms:
            _tmp = []
            for m in _pathways[i]:
                _inter = list(set(map(int,_pathways[i][m]['GENE'])).intersection( _neighbours['adjacent']))
                _idx =  [_neighbours['adjacent'].index(j) for j in _inter ]
                
                _k   = sum([sum(_links['real'][n]) for n in _idx])
                _rnd = sum([sum(_links['random'][n]) for n in _idx])
        
                _pathway_len = len(_pathways[i][m]['GENE'])
        
                _max_Links =  min( _pathway_len*_query_len , _pathways[i][m]['FC4'] , _out_degree)
                crosstalk = calcCrossTalk(_k,_max_Links,_rnd, 1000)
                
                
                _tmp.append({'Pathway':_pathways[i][m]['NAME'][0],'CrossTalkB':crosstalk})
            
            _result[i] = _tmp
                    
        _pvalues = []
        for i in _result:
            [_pvalues.append(m['CrossTalkB']['p.value']) for m in _result[i]]
        
        _qvalues = benjaminiHochberg(_pvalues)
        
        for i in _result:
            for m in _result[i]:
                _tmp = _qvalues[0]
                _qvalues.pop(0)
                #m['CrossTalkB'].append(['q.value'])
                m['CrossTalkB']['q.value']=_tmp
        
        #_result = filter(_result,0.05)
        
        return _result
             
    
def filter(data,alpha):
    for i in data:
        for m in range(0,len(data[i]))[::-1]:
            if(data[i][m]['CrossTalkB']['q.value'] > alpha):
                del data[i][m]
            
    return data
    
def benjaminiHochberg(pvalues):
    
    qvalues = []
    sPvalues = sorted(pvalues)
    idx = [i[0] for i in sorted(enumerate(pvalues), key=lambda x:x[1])]

    weight = range(1,len(sPvalues)+1)[::-1]
    for i in range(0,len(sPvalues)):
        tmp = sPvalues[i]*weight[i]
        if(tmp > 1):
            tmp = 1
        qvalues.append(tmp)

    return [qvalues[i] for i in idx] 
    
    
    
def calcCrossTalk(k,n,e,iterations):
    mu =float( e) / float(iterations)
    
    p = mu/float(n)
    if(mu < k):
        type = '+'
    else:
        type = '-'
    
    return {'p.value':stats.binom_test(float(k), n, p),'type':type}