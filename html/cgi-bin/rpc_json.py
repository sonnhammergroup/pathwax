#!/usr/bin/env python

#    author:     ogris.christoph@scilifelab.se
#    execute:    nosetest -v    
#    conf.ini required

import os
import cgi
import cgitb
import jsonrpc2
import simplejson as json
import ConfigParser

import gene_parser
import database
import network

class DataBaseRequest():
    def __init__(self):
        
        self.form_genes = None
        self.form_species = None
        self.form_adjacent = None
        self.form_term = None
        
        #get configurations     
        self.config            =    ConfigParser.ConfigParser()
        
        if('wwwconf' in os.environ):
            self.config.read(os.environ['wwwconf'] + "/conf.ini")
        else:
            self.config.read("cgi-bin/conf.ini")
        
         
        if('wwwdata' in os.environ):       
            self.dir_database = os.environ['wwwdata']
        else:   
            self.dir_database = self.config.get("DataBase","dir")

         #if using webserver.py its relative to webserver.py
        self.instance_database    =    self.config.get("DataBase","instance")
        self.funcoup_URL        =    self.config.get("FunCoup","url")
        
        #create JsonRPC object and functions
        self.rpc    =    jsonrpc2.JsonRpc()
        
        self.rpc['species'] = self.get_species_all
        self.rpc['translate'] = self.parse_gene_ID_all_to_internal_ID
        self.rpc['internalToEnsembl'] = self.parse_gene_ID_internal_to_ensembl_ID
        self.rpc['internalToAlternative'] = self.parse_gene_ID_internal_to_alternative_ID
        self.rpc['neighbour'] = self.get_adjacent
        self.rpc['path'] = self.get_pathways
        self.rpc['sumRandomLinks'] = self.get_random_links
        self.rpc['links'] = self.get_links
        self.rpc['pathwayGraph'] = self.get_pathway_graph
        self.rpc['geneInfo'] = self.get_gene_information
        self.rpc['randomGenes'] = self.get_gene_information

    def get_species_all(self):
        self.species = database.Species(self.instance_database,
                                        self.dir_database)
        return self.species.all_species()
            
    def parse_gene_ID_all_to_internal_ID(self):
        self.funcoup_internal = gene_parser.FuncoupIDParser(self.instance_database,
                                                            self.dir_database,
                                                            self.form_species, 
                                                            self.funcoup_URL) 
        return self.funcoup_internal.fetch_funcoup_internal_ID(self.form_genes)
        
    def parse_gene_ID_internal_to_alternative_ID(self):
        self.funcoup_internal = gene_parser.FuncoupIDParser(self.instance_database,
                                                            self.dir_database,
                                                            self.form_species, 
                                                            self.funcoup_URL) 
        
        return self.funcoup_internal.fetch_funcoup_alternative_ID(self.form_genes,
                                                               self.form_alternative_ID)
        
        
    def parse_gene_ID_internal_to_ensembl_ID(self):
        self.internal_to_enseble = gene_parser.EnsembleIDParser(self.instance_database,
                                                                self.dir_database,
                                                                self.form_species)   
        return self.internal_to_enseble.fetch(map(int, self.form_genes.split(',') ) )

    def get_adjacent(self):
        self.net = network.Network(
                            self.instance_database,
                            self.dir_database,
                            self.form_species)
        
        return self.net.get_adjacent(map(int, self.form_genes.split(',')))
    
    def get_links(self):
        self.net = network.Network(
                            self.instance_database,
                            self.dir_database,
                            self.form_species)
        
        return self.net.get_links(
                                    map(int,self.form_genes.split(',')),
                                    map(int,self.form_adjacent.split(',')))
    
    def get_pathway_graph(self):
        self.net = network.Network(
                            self.instance_database,
                            self.dir_database,
                            self.form_species)
        
        return self.net.get_pathway_graph(
                                    map(int,self.form_genes.split(',')),
                                    map(int,self.form_pathway_genes.split(',')))
    
    
    
    def get_random_links(self):
        self.net = network.Network(
                            self.instance_database,
                            self.dir_database,
                            self.form_species)
        
        return self.net.sum_random_links(
                                    map(int,self.form_genes.split(',')),
                                    map(int,self.form_adjacent.split(',')))   
        
    def get_pathways(self):
        self.pathways = database.Pathways(
                            self.instance_database,
                            self.dir_database,
                            self.form_species)
        
        return self.pathways.search(map(int, self.form_genes.split(',')),map(int, self.form_bound_genes.split(',')), self.form_term.split(',') )
    

    def get_gene_information(self):
        self.genome = database.Genome(
                        self.instance_database, 
                        self.dir_database, 
                        self.form_species    )
        
        return self.genome.gene_info(map(int,     form.getvalue('query').split(',')))
    
    def get_gene_information(self):
        self.genome = database.Genome(
                        self.instance_database, 
                        self.dir_database, 
                        self.form_species    )
        
        return self.genome.random_genes( int(form.getvalue('query')))
    
        
    def rpc_call(self,form):           
        jsonRpcV = form.getvalue('jsonrpc')
        method = form.getvalue('method')
        requestID = form.getvalue('id')

        _call = {'jsonrpc':jsonRpcV, 'method':method , 'id':requestID}    
        
        self.form_species = form.getvalue('species')
        self.form_genes = form.getvalue('query')
        self.form_adjacent = form.getvalue('neighbour')
        self.form_bound_genes = form.getvalue('bound')
        self.form_term = form.getvalue('type')
        self.form_pathway_genes = form.getvalue('pathway')
        self.form_alternative_ID = form.getvalue('alternativeID')
        
        return self.rpc(_call)


if __name__ == '__main__':

    form = cgi.FieldStorage()
    request = DataBaseRequest()
    
    #execute function and dump result
    print "Content-type: application/json\n"
    print 
    print json.dumps( request.rpc_call(form))

    