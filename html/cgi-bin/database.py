#!/usr/bin/env python
# author: christoph.ogris@scilifelab.se

import os
import simplejson as json
import random

import network

class Species():
    
    def __init__(self, instance_database, dir_database):
        """   
        INPUT:
            1) funcoup instance used
            2) absolute path to data base
        """
        self.instance_database = instance_database
        self.dir_database = "%s/%s" % (dir_database, self.instance_database)     
        # check if species file in DB
        if os.path.isdir(self.dir_database ) == False:
            raise TypeError("[ERROR] Database %s does not exist" % self.dir_database )
        
        self.file_map = "%s/species.map" % (self.dir_database )
        
        if(os.path.isfile(self.file_map) == False):
            raise IOError("[ERROR] cannot read: " + self.file_map)
        
        self.species_header = []
        self.species_dict = {}
        
        self.query = []



    def fetch_species(self,query):
        self.query = query
        
        _found = False
        #if(isinstance(query, str)):

        _file_stream = open(self.file_map, 'r')
    
        # iterate over species
        for line in _file_stream.readlines(): 
            
            #read header
            if (line[0][0] == "#") & (not bool(self.species_header)):
                all_columns = line.split('\t')
                for column in all_columns:
                    self.species_header.append(column.replace("#", "").rstrip())
                    
            if line[0][0] != "#":
                if(len(self.species_header) == 0):
                    raise TypeError("please include header to" % self.file_map )
                
                tmp = {}
                all_columns = line.split('\t')
                for column in all_columns:
                        tmp[self.species_header[all_columns.index(column)]] = column.rstrip()
                        if(str(column.rstrip()) == str(self.query) ):
                           _found = True
                
                if(_found):
                    return tmp
        return None

    def all_species(self):       
        """
        Reads all existing species in table
        OUTPUT: 
            1) json structure  
            example:
            {0: 
                {'long': 'Ciona intestinalis', 
                'short': 'cin', 
                'terms': 'kegg', 
                'taxID': '7719', 
                'fcID': '296239'}
            }  
        """

        idx = 0
        _file_stream = open(self.file_map, 'r')
        
        # iterate over species
        for line in _file_stream.readlines(): 
            
            #read header
            if (line[0][0] == "#") & (not bool(self.species_header)):
                all_columns = line.split('\t')
                for column in all_columns:
                    self.species_header.append(column.replace("#", "").rstrip())
                    
            if line[0][0] != "#":
                if(len(self.species_header) == 0):
                    raise TypeError("please include header to" % self.file_map )
                
                tmp = {}
                all_columns = line.split('\t')
                for column in all_columns:
                        tmp[self.species_header[all_columns.index(column)]] = column.rstrip().split(',')
                
                self.species_dict[idx] = tmp
                idx = idx + 1
                    
        return self.species_dict
        

             
    def get_dir_species(self,query):
        """
        returns the location of the speciesfolder 
        """
        self.species = self.fetch_species(query)
    
        # check if species is available
        self.dir_species     =     "%s/%s" % (self.dir_database, self.species['fcID'])
        if    os.path.isdir(self.dir_species)     ==     False:
            raise TypeError("[ERROR] Species %s does not exist"          % self.dir_species)
          
        return self.dir_species  
    



class Genome():
    def __init__(self, instance_database, dir_database, species_name):
        """
        INPUT:
            1) database instance
            2) database absolute path
            3) some species ID
        """
        self.species = Species(instance_database,dir_database)
        self.species_dir = self.species.get_dir_species(species_name)

        self.file_map = "%s/gene.map" % (self.species_dir)
        self.file_info = "%s/genome.json" % (self.species_dir)
        
        if(os.path.isfile(self.file_map) == False):
            raise IOError("[ERROR] cannot read: " + self.file_map)
        if(os.path.isfile(self.file_info) == False):
            raise IOError("[ERROR] cannot read: " + self.file_info)
        
        
        
        self.genome = []
    
    def iterator(self):
        """
            iterating over genome
            call: for gene in iterator():
        """
        try:
            self.json_genome = open(self.file_map,'r')
            #load lines in chunks into heap memory
            while 1:
                lines = self.json_genome.readlines(50)
                #breakpoint of while loop
                if len(lines) == 0:
                    break
    
                while (len(lines) > 0):
                    line = lines[0].strip().split("\t")            
                    
                    yield line        
                                 
                    del lines[0]
            
        except:
            raise
    
    def random_genes(self,tmp):
        """
        INPUT:
            1) tmp .... number of genes nood to be picked randomly
        """
        with open(self.file_info) as _file:    
            _genome = json.load(_file)
          
        _query = []    
        
        for i in xrange(tmp):
            _query.append(str(_genome[random.choice(_genome.keys())]['ID']['ensembl'][0]))
        
        sep= ','
        
        _result = sep.join(_query)
        
        return _result
    
    def gene_list(self):
            #read genome
        _file    =    open(self.file_map, 'r')

        #iterate over file
        for line in _file:
            _element    =    line.split('\t')
            self.genome.append(    int(_element[0])    )
            #if line has not 2 elements throw error - can occure due to wrong mapping
            if len(line)<2:
                raise TypeError("[ERROR] Genome data corrupt")
        
        return self.genome
    
    def gene_info(self,    query):
        with open(self.file_info) as data_file:    
            _genome = json.load(data_file)
        
        _result = {}
                
        for i in query:
            _result[i] = _genome[str(i)]
                        
        return _result

    
class Network():   
    
    def __init__(self, instance_database, dir_database, species_name):
        self.instance_database  = instance_database
        self.dir_database = dir_database
        self.species_name = species_name
        
        self.species = Species(self.instance_database,
                               self.dir_database)
        
        self.species_dir = self.species.get_dir_species(species_name)
        
        self.genome = Genome(
                                self.instance_database, 
                                self.dir_database, 
                                self.species_name    )
        
        
        self.dir_genes = "%s/genes/" % (self.species_dir)
        if    os.path.isdir(self.dir_genes)     ==     False:
            raise TypeError("[ERROR] Genes %s does not exist"          % self.dir_genes)


    def genes_adjacent(self,gene):
        
        _gene_file_name = "%s/%s" % (self.dir_genes, gene)
         
        if os.path.isfile(_gene_file_name):
            #preparing genelist and file
            _file = open(_gene_file_name)
            _lines_read = 0
            _values = []
              
            #iterate over gene file and find functional associations
            while 1:
                _lines = _file.readlines(10)
                _bools = [i.find('\t1\n')!=-1 for i in _lines]
                _match = [i for i, _elements in enumerate(_bools, 1) if _elements]
        
                if len(_match) != 0:
                    _lineNo = [i+_lines_read-1 for i in _match]
                    _values += _lineNo
        
                _lines_read += len(_lines)
                if len(_lines) == 0:
                    return _values
        else:
            return None

    def valid_gene(self,gene):
        return os.path.isfile("%s/%s" % (self.dir_genes, gene))
    
    #function speeding up file reading. directly jump to needed files.
    #reads lines in chunks which can be processed better in L1 memory
    #tweack: nr -1 otherwise it will run out of index
    def gene_links( self, gene, tmp):
        
        _gene_file_name = "%s/%s" % (self.dir_genes, gene)
        _lines_read    =    0
        _values        =    []
        _rowList = [i+1 for i in tmp]
        
        if(os.path.isfile(_gene_file_name)):
        #if(1==1): 
            #preparing genelist and file
            _file =    open(_gene_file_name,'r',100)
                
            #infinite loop as soon as end of file is reached it will end
            while 1:
                _lines        =    _file.readlines(10)
                
                #iterate over lines in chunks
                while (_lines_read + len(_lines) >= _rowList[0] ):
                    _values.append(_lines[_rowList[0] - _lines_read-1] )
                    
                    del _rowList[0]
                    
                    if len(_rowList)    ==    0:
                        return _values
            
                _lines_read    +=    len(_lines)
                if len(_lines)    ==    0:
                    raise TypeError("[ERROR] gene data corrupt " + str(_rowList[0]))
        else:
            _values = ['0\t0']*len(_rowList)
            return _values
    
class Pathways():   
    
    def __init__(self, instance_database, dir_database, species_name) :
        
        self.instance_database  = instance_database
        self.dir_database = dir_database
        self.species_name = species_name
        
        self.species = Species(self.instance_database,
                               self.dir_database)
        
        self.species_dir = self.species.get_dir_species(species_name)
        
        self.genome = Genome(
                                self.instance_database, 
                                self.dir_database, 
                                self.species_name    )
        
        self.dir_pathways = "%s/pathway/" % (self.species_dir )
        
        #TODO DIRTY HACK TO REPORT RANDOM GENES  
        
        self.net = network.Network(
                            self.instance_database,
                            self.dir_database,
                            self.species_name)
        
        
    def search(self, query_genes, bound_genes, terms):
        self.terms = terms
        self.query_genes = query_genes
        self.bound_genes = bound_genes
        
        _result = {}
        for term in self.terms:
            _tmp_result = {}
            
            _term_file_name = "%s/%s.json" % (self.dir_pathways, str(term))    
            
            with open(_term_file_name) as data_file:    
                _term_data = json.load(data_file)
            
            for i in _term_data:   
                _pathway_genes = map(int , _term_data[i]['GENE'])
                _intersection_length = len(set(_pathway_genes).intersection(self.query_genes))
                if(_intersection_length > 0):
                    _term_data[i]['RANDOM_LINKS'] = 1000#self.net.sum_random_links(self.bound_genes,_pathway_genes)
                    _tmp_result[i] =_term_data[i]
                    
                    
                    
            _result[term]=_tmp_result
        
        return _result
        
    def get_pathways(self, query_genes, bound_genes, terms):
        self.terms = terms
        self.query_genes = query_genes
        self.bound_genes = bound_genes
        
        _result = {}
        for term in self.terms:
            _tmp_result = {}
            
            _term_file_name = "%s/%s.json" % (self.dir_pathways, str(term))    
            
            with open(_term_file_name) as data_file:    
                _term_data = json.load(data_file)
            
            for i in _term_data:   
                _pathway_genes = map(int , _term_data[i]['GENE'])
                _intersection_length = len(set(_pathway_genes).intersection(self.query_genes))
                if(_intersection_length > 0):
                    _tmp_result[i] =_term_data[i]
                               
            _result[term]=_tmp_result
        
        return _result
              
        
        
        
        
        
        
        