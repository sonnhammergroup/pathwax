function GraphView(parent) {
		
	this.vizMethod='forcefield'
	this.linkLimit = 1000;	
	this.force = true;	
	this.initForce = 1;
	this.margin = {
		top : -5,
		right : -5,
		bottom : -5,
		left : -5
	};
	this.width = d3.select("#pathway_viewer").node().getBoundingClientRect().width - this.margin.left - this.margin.right,
	this.height = d3.select("#tab_detail_row").node().getBoundingClientRect().height - this.margin.top - this.margin.bottom;
	
	
	this.div = {
		'parent' : parent,
		'id' : 'pathway_viewer'
	};
}

GraphView.prototype.init = function(data) {
	
	
	if(data.links.length > this.linkLimit)
		this.force = false;
		
	this.preprocessData(data);
	this.draw(); 
	
	if(!this.force){
		this.grid();
	}
	
};

GraphView.prototype.style = function(obj){
	if(obj)
		this.forcefield();
	else
		this.grid();
};

GraphView.prototype.preprocessData = function(data) {
	
	var self = this;
	self.data = data;
		
	//get link degree
	var tmp_nodes = [];
	self.link_max = 0;
	
	for(var i=0,j=self.data.nodes.length; i<j; i++){
	  tmp_nodes[self.data.nodes[i].id] = 0;
	};
	
    for(var z=0,y=self.data.links.length; z<y; z++){
	  tmp_nodes[self.data.links[z].source] +=1;
	  tmp_nodes[self.data.links[z].target] +=1;
	};
	for(var z=0,y=self.data.nodes.length; z<y; z++){
	  self.data.nodes[z].degree = tmp_nodes[self.data.nodes[z].id];
	  if(self.data.nodes[z].degree == 0){
	  	self.data.nodes[z].degree = 1;
	  }
	  if(self.link_max < self.data.nodes[z].degree){
	  	self.link_max = self.data.nodes[z].degree;
	  }
	};
	
	// get genes and calc radius
	
	for(var z=0,y=self.data.nodes.length; z<y; z++){
		//radius
		self.data.nodes[z].radius = (self.data.nodes[z].degree/self.link_max)*20 + 3; 
		
		//get gene obj
		var elemGene = global.RESULT.query.translated.get(self.data.nodes[z].id)
		if(!Boolean(elemGene)){        
				elemGene = global.GENESET.get(self.data.nodes[z].id)   
                }        
		self.data.nodes[z].gene = elemGene
	}
	
	firstBy=(function(){function e(f){f.thenBy=t;return f}function t(y,x){x=this;return e(function(a,b){return x(a,b)||y(a,b)})}return e})();
	self.data.nodes.sort(
		    firstBy(function (a, b) { 
						var tmp_a;
					var tmp_b;
					if(a.group == "signature"){
						tmp_a = 1;   	 			
					}
					if(a.group == 'overlap'){
						tmp_a = 2;   	 			
					}
					if(a.group == 'pathway'){
						tmp_a = 3;   	 			
					}
					
					if(b.group == 'signature'){
						tmp_b = 1;   	 			
					}
					if(b.group == 'overlap'){
						tmp_b = 2;   	 			
					}
					if(b.group == 'pathway'){
						tmp_b = 3;   	 			
					}
					return tmp_a - tmp_b;}
					)
    		.thenBy(function (a, b) { return b.degree - a.degree; }
					)
	);
	
	/*
	//sort degree
	 self.data.nodes.sort(function(a, b) {
            return b.degree - a.degree;
        });
     
    //sort groups   
   	 self.data.nodes.sort(function(a, b) {
   	 		var tmp_a;
   	 		var tmp_b;
   	 		if(a.group == "signature"){
				tmp_a = 1;   	 			
   	 		}
   	 		if(a.group == 'overlap'){
				tmp_a = 2;   	 			
   	 		}
   	 		if(a.group == 'pathway'){
				tmp_a = 3;   	 			
   	 		}
   	 		
   	 		if(b.group == 'signature'){
				tmp_b = 1;   	 			
   	 		}
   	 		if(b.group == 'overlap'){
				tmp_b = 2;   	 			
   	 		}
   	 		if(b.group == 'pathway'){
				tmp_b = 3;   	 			
   	 		}
            return tmp_a - tmp_b;
        });
	*/
	// get grid position

	var square = 27;
	var col_max = Math.round(self.width / 60);
	var elem = 1;
	var row = 0;
	var tmp_group = self.data.nodes[0].group;
	
	for(var z=0,y=self.data.nodes.length; z<y; z++){	
		
	   if(self.data.nodes[z].group != tmp_group ){
	   	    row = row + 2;
	    	elem = 1;
	   }
	   
	   var col = elem%col_max;
	    if(col == 0){
			elem = 1;
			row = row + 1;
			
			
		}		
		self.data.nodes[z].grid = {};
		self.data.nodes[z].grid.x = (square*2) + (elem*(square*2)+ self.width/(square))  ;
		self.data.nodes[z].grid.y = (square*2) + (row*(square*2)) ;
		
		tmp_group = self.data.nodes[z].group;
		elem = elem + 1;		
	}

	//console.log(self.data);
};


GraphView.prototype.updateLayout = function() {		
	var self = this;
	
	self.nodeTransparent
	    .attr("class", 'circle')
        .attr('fill-opacity', 0)
	    .attr("r",  function(d){return self.node.layout.circleSize(d)} );
	
	self.nodeCircle
	    .transition(500)
	    .attr("r",  function(d){return self.node.layout.circleSize(d)} )
	    .attr("class", 'circle')
		.style("fill", self.node.layout.circleColor );
		
	self.nodeText
	    .transition(500)
	    .style("text-anchor", "middle")
  		.style("font-size", "11px")
  		.attr("transform", self.node.layout.labelAngle)
		.text(self.node.layout.labelText );
		
}

GraphView.prototype.setNodeLabel = function (value) {

	var self = this;
//	var requestLabel = false;
	
 	if(value == 'Ensembl'){
		self.node.layout.labelText = function(d) { return d.gene.name; }	
 	}else if(value == 'none'){
		self.node.layout.labelText = function(d) { return ''; }	
 	}else if(value == 'degree'){
		self.node.layout.labelText = function(d) { return d.degree; }	
	}else{
		self.node.layout.labelText = function(d) { 
			if(d.group == 'pathway')
				return d.gene.alternativeID.get(value); 
			else
				return d.gene.name;
			}
	}

	self.updateLayout();	
	
 	
}

GraphView.prototype.setNodeSize = function (value) {
	var self = this;
 	if(value == 'linkdegree'){
		 self.node.layout.circleSize  =   function(d) { return d.radius; };	
 	}
 	if(typeof value == 'number'){
		 self.node.layout.circleSize  =  function(d) { return value; };	
 	}
 	self.updateLayout();
}

GraphView.prototype.draw = function() {
		var self = this;
		
	var svg = d3.select("#"+self.div.id)
					.append("svg")
						.attr("width", self.width + self.margin.left + self.margin.right)
						.attr("height", self.height + self.margin.top + self.margin.bottom)
	    .call(d3.zoom()
	     .scaleExtent([0.3, 10])
	     .on("zoom", zoomed));
 		
	self.container = svg.append("g");	
 					
	function zoomed() {
	  self.container.attr("transform", d3.event.transform);
	}  
						
						

	function color(group){
		if(group == 'signature')
			return '#74c476'
		if(group == 'overlap')
			return '#9e9ac8'
		if(group == 'pathway')
			return '#ffa500'
	}//d3.scaleOrdinal(d3.schemeCategory10);

	self.link = self.container.append("g")
					.attr("class", "links")
						.selectAll("line")
						.data(self.data.links).enter()
						.append("line");
							
	self.node = self.container.append("g")
					.attr("class", "nodes")
					.selectAll("nodes")
					.data(self.data.nodes).enter()
						.append("g")
						  //  .on('mouseover', mouseOver)
						  //  .on('mouseout', mouseOut) 
							.call(d3.drag()
										.on("drag", dragmove));		
										
										
	self.node.layout = { circleSize: function(d) {  return d.radius; },
						 circleColor: function(d) { return color(d.group); },
						 //labelText: function(d) { return d.gene.name; },
						 labelText: function(d) { return ''; },
						 labelAngle: "rotate("+0+")" 
	 };	
	
	self.nodeCircle = self.node.append("circle");
	
	self.nodeText = self.node.append("text")
	
	self.nodeTransparent = self.node.append("circle")
							    .on('mouseover', mouseOver)
							    .on('mouseout', mouseOut) ;
	
	self.updateLayout();	

												
	self.simulation = d3.forceSimulation();
	
	self.simulation.parameter = {
		'distance' : d3.forceLink().id(function(d) {return d.id;}),
		'charge' : d3.forceManyBody(),
		'collide' : d3.forceCollide().radius(function(d) {return d.radius + 10;}),
		'center' : d3.forceCenter(self.width / 2, self.height / 2)
	};
	

	self.simulation.force("link", self.simulation.parameter.distance.distance(60))
					.force("charge", self.simulation.parameter.charge.strength(-400))
					.force("collide", self.simulation.parameter.collide.iterations(20))
					.force("center", self.simulation.parameter.center);
	
	self.simulationNodes = self.simulation.nodes(self.data.nodes);
	self.simulation.force("link").links(self.data.links);
	
		
	function dragmove(d, i) {
		
		d3.select(this).style("cursor", "pointer");
		
    	if(self.vizMethod=='grid'){	
	        d.grid.px += d3.event.dx;
	        d.grid.py += d3.event.dy;
	        d.grid.x += d3.event.dx;
	        d.grid.y += d3.event.dy;
	        tick(); 
		}
		if(self.vizMethod=='forcefield'){	
	        d.px += d3.event.dx;
	        d.py += d3.event.dy;
	        d.x += d3.event.dx;
	        d.y += d3.event.dy; 
	        tick(); // this is the key to make it work together with updating both px,py,x,y on d !
		}
    }
	
	function tick() {
		if(self.vizMethod=='grid'){
      	self.link.attr("x1", function(d) { return d.source.grid.x; })
          .attr("y1", function(d) { return d.source.grid.y; })
          .attr("x2", function(d) { return d.target.grid.x; })
          .attr("y2", function(d) { return d.target.grid.y; });

	      self.node.attr("transform", function(d) { return "translate(" + d.grid.x + "," + d.grid.y + ")"; }); 
		}
		
		if(self.vizMethod=='forcefield'){
		      self.link.attr("x1", function(d) { return d.source.x; })
	          .attr("y1", function(d) { return d.source.y; })
	          .attr("x2", function(d) { return d.target.x; })
	          .attr("y2", function(d) { return d.target.y; });
	          
	      self.node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });   
	    	
		}
		
    };
	    
	
	
	if(self.force){	  

	self.simulationNodes.on("tick", tick); 	
	self.simulation.alphaMin(0.3);
	}
	
	self.linkedByIndex = {};
	self.data.links.forEach((d) => {
	  self.linkedByIndex[`${d.source.index},${d.target.index}`] = true;
	}); 
	
	
	function mouseOver(d) {
	 //const object = d3.select(this); 
	  	self.node.transition(500)
	      .style('opacity', o => {
	        if ( self.linkedByIndex[`${o.index},${d.index}`] || self.linkedByIndex[`${d.index},${o.index}`] || o.index === d.index) {
	          return 1.0;
	        }
	        return 0;
	      });
	      
		  self.link.transition(500)
		      .style('stroke-opacity', o => (o.source === d || o.target === d ? 0.8 : 0));	
		
	};
 
	function mouseOut() {
	   self.node
	    .transition(500)
	    .style('opacity',1);
	    
	  self.link
	    .transition(500).style('stroke-opacity',0.4);      
	};
	
	
	//self.setNodeLabel('$("#nodeLableID").val()');
	
	
};

 
GraphView.prototype.grid = function() {
	var self = this;
	self.simulation.stop();
	self.vizMethod='grid';
	
	self.node.layout.labelAngle = "rotate("+-30+")" ;
	
 	self.updateLayout();
	
	self.link.transition().duration(750)
		.attr("x1", function(d) {return d.source.grid.x;})
		.attr("y1", function(d) {return d.source.grid.y;})
		.attr("x2", function(d) {return d.target.grid.x;})
		.attr("y2", function(d) {return d.target.grid.y;});
		
	self.node.transition().duration(750)
			.attr("transform", function(d) { return "translate(" + d.grid.x + "," + d.grid.y + ")"; }); 	
};

GraphView.prototype.forcefield = function() {

	var self = this;

	
	self.node.layout.labelAngle = "rotate("+0+")" ;
 	self.updateLayout();
	
    self.vizMethod='forcefield';
		self.link.transition().duration(750)
			.attr("x1", function(d) {return d.source.x;})
			.attr("y1", function(d) {return d.source.y;})
			.attr("x2", function(d) {return d.target.x;})
			.attr("y2", function(d) {return d.target.y;});
			
		self.node.transition().duration(750).attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; }); 	
	
};
