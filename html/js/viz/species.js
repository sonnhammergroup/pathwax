loadSpeciesInterface = function(parent) {
    var self = this;
    self.parent = parent;
    self.id = 'species_table';
    
    if(!global.DATABASE._status)
        return false;
        
    //console.log(self.parent);
    
    var cardcontent = $('<div>', {
        class : 'card-content',
        id : self.id
    });
    
    var title = $('<div>', {
        class : 'row',
        //html : "<i class='small material-icons left'>toc</i><h5>Select species</h5>"
    });
    
    cardcontent.append(title);

    var card = $('<div>', {
        class : ''
    });
      
    var container = $('<div>', {
        class : 'container'
    });
    
    
    card.append(cardcontent);
    container.append(card);
    
    $('#'+self.parent).append(container);

    
    var table = d3.select($('#' + self.id).selector).append("ul")
                        .attr('class', "collection blue-grey-text text-darken-3") ;
    
    T = table.selectAll('.li').data(global.DATABASE.species).enter()
            .append("li")
                .attr('class', "collection-item avatar")
                .attr('onclick', function(d){ return ' global.action.submitSpecies('+String(d.id[0])+')';});
                
    T.append("img")
            .attr('class', "circle")
            .attr('src', function(d){return 'img/species/'+String(d.taxID[0])+'.jpg';}); 
            
    T.append("span")
            .attr('class', "title")
            .text(function(d){return String(d.name[0]);});
            //.text(function(d){return String(d.name[0]) +' ('+String(d.other[0])+')';});  
    
    //<span class="new badge">1</span>
    T.append("div")
            .append("span")
            .attr('class', "badge")  
            .attr("hidden",function(d){
                if(d.example[0] == 'null' )
                    return true;
                else
                    return null;
                
                })            
                .text(function(d){return String(d.example.length) + ' Example';});  
  
                    
    T.append("p")
                    .text(function(d){return 'TaxID: ' + String(d.taxID[0]);});  
                    
    T.append("p")
                    .text(function(d){return 'aka: ' + String(d.other[0]);}); 
                    
    
};


