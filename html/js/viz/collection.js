/* ****************** ***********************
 *    	  Result builder
 ******************** ******************** */

function Interface(parent,parentViewer) {
	
	this.data = global.RESULT.result;

    //clear parent
    $('#' + parent).empty();

    //init structure
    var row = $('<div>', {
        class : 'row'
    });
    row.append($('<div>', {
        class : 'col l2',
        id : 'setting'
    }));
    row.append($('<div>', {
        class : 'col l10',
        id : 'main'
    }));

    $('#' + parent).append(row);

    //init listener
    var dispatch = d3.dispatch('update');

    var pathwayViewer = new PathwayViewer(parentViewer,this.data);
    dispatch.on('update.pathwayViewerList',function(){pathwayViewer.updateList();});
    
    
    //init MainStatistics
    var mainStatistics = new MainStatistics('main',this.data);
    dispatch.on('update.KEGG',function(){mainStatistics.listener.call('update');});
    
    mainStatistics.listener.on('switchToDetails',function(pathwayID){
		$('ul.tabs').tabs('select_tab', 'tab_detail');
		$("#optionpathway").val(pathwayID);
    	pathwayViewer.init(pathwayID);
    	});
    
    //init settings
    var option = new Settings('setting');
    option.dispatcher = dispatch;
    option.filter();
    
	$('.toggleFieldset').click(function () {
	    $(this).parent().find('.hiders').toggle();
	    var value = $(this).text();
	    if (value.indexOf('[+]') >= 0) value = value.replace('[+]', '') + ' [-]';
	    else value = value.replace('[-]', '') + ' [+]';
	    $(this).html(value);
		});
		  
};

/* ****************** ***********************
 *    	  Main stats
 ******************** ******************** */

function MainStatistics(parent,data) {
	
	var dbname = "";
	if($('#termsetSelection').val() == 0){
    	dbname = "KEGG";
    }else{
    	dbname = "Reactome";
    }
    
    var self = this;

    this.data = data;
    
    var cardcontent = $('<div>', {
        class : 'card-content'
    });


    cardcontent.append($('<div>', {
        class : 'row',
        html : "<i class='small material-icons left'>assessment</i><h5>"+dbname+" pathways with crosstalk to the query</h5>"
    }));
    
    var overview =  $('<div>', {
        class : 'row'
    });
    overview.append($('<div>', {
        class : 'col l6',
        id : parent + '_legend'
    }));
    overview.append($('<div>', {
        class : 'col l6',
        id : parent + '_pie'
    }));
    cardcontent.append(overview);
    
    
    var heatmap = $('<div>', {
        class : 'row'
    });
    heatmap.append($('<div>', {
        id : parent + '_heatmap'
    }));
    
    cardcontent.append(heatmap);

    var card = $('<div>', {
        class : ''
    });
    card.append(cardcontent);
    
        
        
    var row =  $('<div>', {
        class : 'row'
    });
    row.append(card);
     
    $('#' + parent).append(row);



    
    this.listener = d3.dispatch('update','classSelector','mouseover','mouseout','switchToDetails');
    
    
    //overviewDataParser
    this.overviewParser = new OverviewDataParser(this.data);       
    this.listener.on('update.overviewParser',function(){self.overviewParser.update();});
    
    //overviewTable
    this.legend = new Legend(this.overviewParser, parent + '_legend');
    this.legend.eventListener = this.listener;
    this.listener.on('update.legend',function(){self.legend.update();});
    this.listener.on('mouseover.legend',function(term){self.legend.mouseover(term);});
    this.listener.on('mouseout.legend',function(term){self.legend.mouseout(term);});
    
    //PieChart
    this.pieChart = new PieChart(this.overviewParser, parent + '_pie');
    this.pieChart.eventListener = this.listener;
    this.listener.on('update.pieChart',function(){self.pieChart.update();});
    this.listener.on('mouseover.pieChart',function(term){self.pieChart.mouseover(term);});
    this.listener.on('mouseout.pieChart',function(term){self.pieChart.mouseout(term);});

    //heatmap
    this.heatmap = new HeatMap(this.data, parent + '_heatmap');
    this.heatmap.eventListener = this.listener;
    this.listener.on('update.heatmap',function(){self.heatmap.updateFDR(self.data);self.heatmap.validate();}); 
    this.listener.on('classSelector.heatmap',function(term){self.heatmap.selectTermClass(term);});
    this.listener.on('switchToDetails',function(pathwayID){self.selectPathway(pathwayID);});
     
}

MainStatistics.prototype.selectPathway = function(){};


/* ****************** ***********************
 *    	   Result:Details PathwayViewer
 ******************** ******************** */



function PathwayViewer(parent,data){
	
    this.data = data;
    
	//clear parent
    $('#' + parent).empty();

    //init structure
    var row = $('<div>', {
        class : 'row'
    });
    row.append($('<div>', {
        class : 'col l2',
        id : 'pathway_setting'
    }));
    row.append($('<div>', {
        class : 'col l10',
        id : 'pathway_main'
    }));

    $('#' + parent).append(row);
    
    //init save
  	this.setting('pathway_setting');
} 

PathwayViewer.prototype.setting = function(parent){
	var self = this;
	
    var card = $('<div>', {
        class : ''
    });
    
    var cardcontent = $('<div>', {
        class : 'card-content'
    });

    var title = $('<div>', {
        class : 'row',
        html : "<i class='small material-icons left'>settings</i><h5>Statistics</h5>"
    });
    
    cardcontent.append(title);
    
    
    var termselect = {
        fieldset : $('<fieldset>', {
            html : '<legend>Select Pathway</legend>'
        }) ,
        Pathway : $('<div/>', {
            class : 'row',
            html : "<select id='optionpathway' class='browser-default'></select>"
        }),
        dbLinks : $('<div/>', { 
            class : 'row',
            html : '<div id="dbLinks" style="width: 100%;overflow: hidden;"><div style="width: 50%;float:left;"><a id="fcLink"><i class="tiny material-icons left" style="margin: 5px 1px 0px 5px;">language</i>FunCoup</a></div><div style="overflow: hidden;"><a id="pathwayLink"><i class="tiny material-icons left" style="margin: 5px 1px 0px 5px;">language</i>'+ 
            ($('#termsetSelection').val()==0 ? 'KEGG' : 'Reactome')         
             +'</a></div></div>'
        })
    };
	
    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    
    termselect.fieldset.append(termselect.Pathway);
    termselect.fieldset.append(termselect.dbLinks);
    rowWrapper.append(termselect.fieldset);
    cardcontent.append(rowWrapper);    
  
    	var statsCrossTalk = {
        fieldset : $('<fieldset>', {
            html : '<legend>Analysis</legend>'
        }) ,
        SharedLinks : $('<div/>', {
            class : 'row',
            html : "<div id='stats_SharedLinks'>Crosstalk: 0 </div> "
        }),
        ExpectedLinks : $('<div/>', {
            class : 'row',
            html : "<div id='stats_ExpectedLinks'>Expected crosstalk: 0 </div> "
        }),
        Relation : $('<div/>', {
            class : 'row',
            html : "<div id='stats_Relation'>Type: - </div> "
        }),
        FWER : $('<div/>', {
            class : 'row',
            html : "<div id='stats_FWER'>FWER: 0 </div> "
        })
    };
	
    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    

    statsCrossTalk.fieldset.append(statsCrossTalk.FWER);
    statsCrossTalk.fieldset.append(statsCrossTalk.Relation);
    statsCrossTalk.fieldset.append(statsCrossTalk.SharedLinks);
    statsCrossTalk.fieldset.append(statsCrossTalk.ExpectedLinks);
    rowWrapper.append(statsCrossTalk.fieldset);
    cardcontent.append(rowWrapper);  
   
  	card.append(cardcontent);
    var statsAdvanced = {
        fieldset : $('<fieldset>', {
            html : '<legend class="toggleFieldset"  style="cursor:pointer">More [+]</legend>'
        }) ,
        hiders : $('<div>', {
            class : 'hiders',
            style : 'display:none'
        }) ,
        SignatureSize : $('<div/>', {
            class : 'row',
            html : "<div id='stats_SignatureSize'>Genes in query:  0</div> "
        }),
        PathwaySize : $('<div/>', {
            class : 'row',
            html : "<div id='stats_PathwaySize'>Genes in pathway:  0</div> "
        }),
        Overlap : $('<div/>', {
            class : 'row',
            html : " <div id='stats_OverlapSize'>Shared genes: 0</div> "
        }),
        SignatureDegree : $('<div/>', {
            class : 'row',
            html : "<div id='stats_SignatureDegree'>Query linkdegree:  0</div> "
        }),
        PathwayDegree : $('<div/>', {
            class : 'row',
            html : "<div id='stats_PathwayDegree'>Pathway linkdegree: 0 </div> "
        }),
        NetworkRandom : $('<div/>', {
            class : 'row',
            html : "<div id='stats_NetworkRandom'>Network randomizations: 0 </div> "
        }),
        MaxCrosstalk : $('<div/>', {
            class : 'row',
            html : "<div id='stats_RandomLinks'>Max. crossTalk:  0</div> "
        }),
        pVal : $('<div/>', {
            class : 'row',
            html : "<div id='stats_pVal'>p-value(uncorrected):0  </div> "
        })
    };
	
    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    
    statsAdvanced.hiders.append(statsAdvanced.PathwaySize);
    statsAdvanced.hiders.append(statsAdvanced.Overlap);
    statsAdvanced.hiders.append(statsAdvanced.SignatureDegree);
    statsAdvanced.hiders.append(statsAdvanced.PathwayDegree);
    statsAdvanced.hiders.append(statsAdvanced.NetworkRandom);
    statsAdvanced.hiders.append(statsAdvanced.MaxCrosstalk);
    statsAdvanced.hiders.append(statsAdvanced.pVal);
    statsAdvanced.fieldset.append(statsAdvanced.hiders);
    rowWrapper.append(statsAdvanced.fieldset);
    cardcontent.append(rowWrapper);  
  	card.append(cardcontent);
  
    $('#' + parent).append(card);
    
    
    
    var card = $('<div>', {
        class : ''
    });
    
    var cardcontent = $('<div>', {
        class : 'card-content'
    });

    var title = $('<div>', {
        class : 'row',
        html : "<i class='small material-icons left'>settings</i><h5>Visualization</h5>"
    });
    
    cardcontent.append(title);
   
    var viewerStyle = {
        main : $('<fieldset>', {
            html : '<legend>Legend</legend>'
        }) ,
        style : $('<div/>', {
            class : 'row',
            html : '<table style="width:100%"><tr><td>Pathway</td><td>Shared</td><td>Query</td></tr><tr><td><div class="circleLegend" style="background:#ffa500;"></div></td><td><div class="circleLegend" style="background:#9e9ac8;"></div></td> <td><div class="circleLegend" style="background:#74c476;"></div></td></tr></table>'
        })
    };

    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
   
    
    viewerStyle.main.append(viewerStyle.style);
	rowWrapper.append(viewerStyle.main);
  	cardcontent.append(rowWrapper);
  	
   
    var viewerStyle = {
        main : $('<fieldset>', {
            html : '<legend>Gaph Style</legend>'
        }) ,
        form : $('<div/>', {
            class : 'switch'
        }),
        style : $('<div/>', {
            class : 'row',
            html : "<div class='switch'>Grid<label><input type='checkbox' id='checkboxForcefield'> <span class='lever'></span></label>Graph</div> "
        })
    };

    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
   
    
    viewerStyle.main.append(viewerStyle.form); 
    viewerStyle.form.append(viewerStyle.style);
	rowWrapper.append(viewerStyle.main);
  	cardcontent.append(rowWrapper);
  	
  	
  	card.append(cardcontent);
  	
    var viewerStyle = {
        main : $('<fieldset>', {
            html : '<legend class="toggleFieldset" style="cursor:pointer">More [+]</legend>'
        }) ,
        hiders : $('<div>', {
            class : 'hiders',
            style : 'display:none'
        }) ,
        nodelabel : $('<div/>', { 
            class : 'row',
            html : "Pathway node labels:<select id='nodeLableID'  class='browser-default'></select>"
        }),
        save : $('<div/>', { 
            class : 'row',
            html : 'Save graph:<p> <button class="waves-effect waves-light btn" id="saveGraph"><i class="material-icons left">cloud</i>Save</button><p/>'
        })
    };

    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
   
    
    viewerStyle.hiders.append(viewerStyle.nodelabel); 
    viewerStyle.hiders.append(viewerStyle.save); 
    viewerStyle.main.append(viewerStyle.hiders); 
	rowWrapper.append(viewerStyle.main);
  	cardcontent.append(rowWrapper);
  	
  	card.append(cardcontent);  	
  	

  	
    $('#' + parent).append(card); 
    
    
    
	for (var i = 0, j = global.SPECIES.identifierSource.length; i < j; i++) {
		$('#nodeLableID').append('<option value="'+global.SPECIES.identifierSource[i]+'">'+ global.SPECIES.identifierSource[i]+'</option>');	
	};
	
	$('#nodeLableID').val('UniProt');
    
    $('#nodeLableID').append('<option value="none">None</option>');
    $('#nodeLableID').append('<option value="degree">Node degree</option>');
    
	$("#saveGraph").disabled = true;

	self.data.sort(function(a, b) {
            return a.crosstalk.pValue.displayed - b.crosstalk.pValue.displayed ;
        });			
	       
	self.updateList();	
	
	$('#optionpathway').on('change', function() {
        self.init($('#optionpathway').val());    
    });
    
	$('#checkboxForcefield')[0].checked = true;
	
	

};

PathwayViewer.prototype.updateList= function(){
	$("#saveGraph").prop("disabled", true);
    $('#pathway_viewer').remove();
    $( "#pathway_main" ).append( "<div id='pathway_viewer'></div>" );
	$('#optionpathway').find('option').remove();
	$('<option value="" disabled selected>Choose your Pathway</option>').appendTo('#optionpathway');
	this.resetStats();
	
	var tmp_count = 0;
	var self = this;
	for (var i = 0,j =  self.data.length; i < j; i++) {
		if(self.data[i].pathway.name[0] != "" & self.data[i].display){
			$('<option value="'+ self.data[i].pathway.id +'">'+ self.data[i].pathway.name[0] +'</option>').appendTo('#optionpathway');
			if(tmp_count==0)
				tmp_count = i;
		}	
	};	
	 
};

PathwayViewer.prototype.openExtrernalDB = function(){
	var self = this;
	console.log(self.current);
	window.open(getDbLink(self.current));
};

PathwayViewer.prototype.resetStats = function(){

    $('#dbLinks').find('#pathwayLink').css('cursor', 'default').css('color', 'grey').off('click');
    
	$('#stats_PathwaySize').text('Genes in pathway: ');
	$('#stats_OverlapSize').text('Shared genes: ');
	$('#stats_SignatureDegree').text('Query linkdegree: ' );
	$('#stats_PathwayDegree').text('Pathway linkdegree: ');
	$('#stats_SharedLinks').text('Crosstalk: ');
	$('#stats_ExpectedLinks').text('Expected crosstalk: ');
	$('#stats_Relation').text('Type: ');
	$('#stats_FWER').text('Corrected p value: ');
	$('#stats_NetworkRandom').text('Network randomizations: '  );
	$('#stats_RandomLinks').text('Max. crossTalk: ' );
	$('#stats_pVal').text('p-value(uncorrected): ' );
};

PathwayViewer.prototype.loadStats = function(result){	
	this.resetStats();

	$('#dbLinks').find('#pathwayLink').css('cursor', 'pointer').css('color', '').on( "click", function() {
		$(this).target = "_blank";
		window.open(getDbLink(result.pathway.id));
		return false;
	});
	
	$('#stats_PathwaySize').text('Genes in pathway:'+result.pathwaySize);
	$('#stats_OverlapSize').text('Shared genes: ' + result.overlap.size);
	$('#stats_SignatureDegree').text('Query linkdegree: ' + result.outdegreeSignature);
	$('#stats_PathwayDegree').text('Pathway linkdegree: ' + result.outdegreePathway);
	$('#stats_SharedLinks').text('Crosstalk: ' + result.crosstalk.k);
	$('#stats_ExpectedLinks').text('Expected crosstalk: ' + Math.round(result.crosstalk.mean));
	$('#stats_Relation').text('Type: ' + result.crosstalk.type);
	
	var correctionMethod = $('#correctionMethod').val();
	if(correctionMethod=='value')
		$('#stats_FWER').text('pValue: ' + result.crosstalk.pValue.value.toPrecision(3));
	else{	
		$('#stats_FWER').text(correctionMethod.toUpperCase()+': ' + result.crosstalk.pValue.displayed.toPrecision(3));
	}
	$('#stats_NetworkRandom').text('Network randomizations: ' + result.crosstalk.iterations);
	$('#stats_RandomLinks').text('Max. crossTalk: ' + result.crosstalk.N);
	$('#stats_pVal').text('p-value(uncorrected): ' + result.crosstalk.pValue.value.toPrecision(3));
};

PathwayViewer.prototype.changeLabels = function(value){	
	$("#nodeLableID").prop("disabled", true);
	
 	var self = this;
	var request = false;
	
	for(var i=0,j=self.graph.data.nodes.length; i<j; i++){
	  if(self.graph.data.nodes[i].gene.alternativeID.get(value) == null){
	  	request = true;
	  }
	};
	
	if(value == 'none' || value == 'degree' || value == 'Ensembl'){
 		self.listener.call('labels','',value);
		
	}else if(request){
	
		var geneset = new Map();
		var translate = new TranslateToAlternative(); 
 	
		translate.updateStatus = function(){
			self.listener.call('labels','',value);
			};
		
		for(var i=0,j=self.graph.data.nodes.length; i<j; i++){
		  var gene = global.GENESET.get(self.graph.data.nodes[i].gene.id);  
		  geneset.set(self.graph.data.nodes[i].gene.id,gene);
		};
		translate.init(geneset,value);	
		
	}else {
		self.listener.call('labels','',value);
	}
	
};



PathwayViewer.prototype.init = function(pathwayID){
	document.body.style.cursor='wait';	
	$("#saveGraph").prop("disabled", true);
    $('#pathway_viewer').remove();
    $( "#pathway_main" ).append( "<div id='pathway_viewer'></div>" );
    
	var self = this;
    delete self.graph;  
    delete self.pathway; 
    delete self.query;  
 	

	
	for(var i=0,j=global.RESULT.result.length; i<j; i++){
	  if(global.RESULT.result[i].pathway.id==pathwayID){
		self.pathway = global.RESULT.result[i].pathway;	  
		self.query = global.RESULT.result[i].query;		
    	self.loadStats(global.RESULT.result[i]); 
	 }
	};
           
                   
	self.graph = new GraphView('pathway_main');
 	self.listener = d3.dispatch('labels');
	self.listener.on('labels.reactivateSelection',function(){$("#nodeLableID").prop("disabled", false);});
	self.listener.on('labels.alternative',function(value){self.graph.setNodeLabel(value);});	
	
	//SUPER messy but does the job. need to refresh the fc link everytime a new pathway is loaded	
	self.listener.on('labels.refreshFunCoupLink',function(value){
		var queryGenes = [];
		var pathwayGenes = [];
		//console.log(self.graph.data);
		if(value == 'Ensembl' || value == 'degree' || value == 'none'){
			for(var z=0,y=self.graph.data.nodes.length; z<y; z++){
				if(self.graph.data.nodes[z].group == 'pathway'){
					pathwayGenes.push(self.graph.data.nodes[z].gene.name);
				}else{
					queryGenes.push(self.graph.data.nodes[z].gene.name);
				}
			};
		}else{
			for(var z=0,y=self.graph.data.nodes.length; z<y; z++){
				
				if(self.graph.data.nodes[z].group == 'pathway'){
					pathwayGenes.push(self.graph.data.nodes[z].gene.alternativeID.get(value));
				}else{
					queryGenes.push(self.graph.data.nodes[z].gene.name);
				}
		};	
		}
		$('#dbLinks').find('#fcLink').css('cursor', 'pointer').css('color', '').click(function() {window.open(FunCoupLinkGenes(queryGenes,pathwayGenes));});
		
		});	    		    	
	
	
	
 	
	var tmp_pathwaygenes = [];
	self.pathway.genes.forEach(function(value, key) {
		              tmp_pathwaygenes.push(key);
		            },tmp_pathwaygenes);
	
	var tmp_querygenes = [];
	self.query.forEach(function(value, key) {
		              tmp_querygenes.push(key);
		            },tmp_querygenes);	
	
    $('#checkboxForcefield').change(function() {	
		self.graph.style($('#checkboxForcefield')[0].checked);
		
	});
	  
	$('#nodeLableID').on('change', function() {
		self.changeLabels($("#nodeLableID").val());
    });
	
    var dataBaseCall = new DataBaseCall();
    dataBaseCall.setMethod('pathwayGraph');
    dataBaseCall.setRequest(tmp_querygenes+ '&pathway=' + tmp_pathwaygenes);
    dataBaseCall.setSpecies(global.SPECIES.id[0]);
    dataBaseCall.update = function(data) {
		  self.graph.init(data);
		    if(!self.graph.force){
		  	  $('#checkboxForcefield').prop("disabled", true);
		  	  $('#checkboxForcefield').prop("checked", true);
		  }else{
		  	  $('#checkboxForcefield').prop("disabled", false);
		  	  $('#checkboxForcefield').prop("checked", true);
		  	  
		  }
		
	    //save
	    $("#saveGraph").prop("disabled", false);
	    $( "#saveGraph" ).click(function() {ExportGraphToCsv('pathwax_graph.csv',self.graph.data);});
			
		document.body.style.cursor='default';
		self.changeLabels($("#nodeLableID").val());
    };
    dataBaseCall.call();
 
		delete tmp_pathwaygenes;
		delete tmp_querygenes;
    
};

