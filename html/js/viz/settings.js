function Settings(parent) {
    var self = this;
    self.data = global.RESULT.result;

    
    var card = $('<div>', {
        class : ''
    });
    
    var cardcontent = $('<div>', {
        class : 'card-content'
    });

    var title = $('<div>', {
        class : 'row',
        //html : "Filter"
        html : "<i class='small material-icons left'>settings</i><h5>Settings</h5>"//<div class='divider'></div>"
    });
    
    cardcontent.append(title);

    //define switches
    var switches = {
        fieldset : $('<fieldset>', {
            id : parent + '_switchesr',
            html : '<legend>Filter</legend>'
        }),
        enrichment : $('<div/>', {
            class : 'row',
            html : "<div class='switch'><label><input type='checkbox' id='checkboxEnrichment'>   <span class='lever'></span></label>   Enrichment</div> "
        }),
        depletion : $('<div/>', {
            class : 'row',
            html : "<div class='switch'> <label><input type='checkbox' id='checkboxDepletion'>   <span class='lever'></span></label> Depletion </div> "
        }),
        onlyOverlap : $('<div/>', {
            class : 'row',
            html : "<div class='switch'> <label><input type='checkbox' id='checkboxOnlyOverlap'>   <span class='lever'></span></label> Overlap only </div> "
        })
    };

    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    

    
    switches.fieldset.append(switches.enrichment);
    switches.fieldset.append(switches.depletion);
    switches.fieldset.append(switches.onlyOverlap);
    
    rowWrapper.append(switches.fieldset);
    cardcontent.append(rowWrapper);
    // not checked
    card.append(cardcontent);



    var p_value = 0.05;
   
    
    var pvalue_NumberInput = {
        main : $('<fieldset>', {
            id : parent + '_pvalue_NumberFilter',
            html : '<legend>Statistics</legend>'
        }) ,
        method : $('<div/>', {
            class : 'col l12',
            html : "<p><label>Multiple hypotheses test correction</label><select id='correctionMethod' class='browser-default'><option value='value'>None</option><option selected value='fwer'>Bonferroni (FWER)</option><option value='fdr'>Benjamini-Hochberg (FDR)</option></select>"
        }) ,
        text : $('<div/>', {
            class : 'col l12',
            html : "<p><label>Cut off</label><input type='number' id='pvalueNumberInput' min='0.0001' max='1' value="+p_value+" step='0.0005'>"
        })
    };
    
    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    
    pvalue_NumberInput.main.append(pvalue_NumberInput.method);
    pvalue_NumberInput.main.append(pvalue_NumberInput.text);
    rowWrapper.append(pvalue_NumberInput.main);
    cardcontent.append(rowWrapper);

    
    //calc
    var nrResults = 0;
    for (var i = 0,
        j = self.data.length; i < j; i++) {
        if (self.data[i].crosstalk.pValue.displayed <= 1) {
            nrResults = nrResults + 1;
        }
    };

    this.textfilterID = id + '_TopResultFilter_TextValue';
    this.sliderfilterID = id + '_TopResultFilter_RangeValue';
    
    //define Filter
    var topResultFilter = {
        fieldset : $('<fieldset>', {
            id : parent + '_TopResultFilter',
            html : '<legend>Max. pathways</legend>'
        }),
        number : $('<div/>', {
            class : 'col l5',
            html : "<input type='number' id=" + this.textfilterID + " min='1' max=" + nrResults + " value=" + nrResults + ">"
        }),
        slider : $('<div/>', {
            class : 'col l7',
            html : "<div class='range-field'><input type='range' id=" + this.sliderfilterID + " min='1' max=" + nrResults + " value=" + nrResults + "></div>"
        }),
    };
    
    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    
    topResultFilter.fieldset.append(topResultFilter.number);
    topResultFilter.fieldset.append(topResultFilter.slider);  
    
    rowWrapper.append(topResultFilter.fieldset);
    
    cardcontent.append(rowWrapper);
    
      var save = {
        main : $('<fieldset>', {
            id : parent + '_save',
            html : '<legend class="toggleFieldset" style="cursor:pointer">More [+]</legend>'
        }) ,
        hiders : $('<div>', {
            class : 'hiders',
            style : 'display:none'
        }) ,
        text : $('<div/>', {
            class : 'col l12',
            html : 'Save as csv: <a class="waves-effect waves-light btn" id="saveMain"><i class="material-icons left">cloud</i>Save</a><p/>'
        })
    };
    
    var rowWrapper = $('<div>', {
        class : 'row'
    }); 
    
    save.hiders.append(save.text);
    save.main.append(save.hiders);
    rowWrapper.append(save.main);
    cardcontent.append(rowWrapper);
   
    //init structure
    $('#' + parent).append(card);

    //TODO UNCHECK IF NOTHING FOUND
    $('#checkboxEnrichment').prop('checked', true);
    $('#checkboxDepletion').prop('checked', false);

    //ONCHANGE
    
    //save
    $( "#saveMain" ).click(function() {ExportStatsToCsv('pathwax_stats.csv',self.data);});

    //topresults
    $('#' + this.sliderfilterID).on('change', function() {
        $('#' + self.textfilterID).val($(this).val());
        self.filter();
    });
    $('#' + this.textfilterID).on('change', function() {
        $('#' + self.sliderfilterID).val($(this).val());
        self.filter();

    });
    //pvalue
    $('#pvalueNumberInput').on('change', function() { 
        self.filter();     
    });    
    
    $('#correctionMethod').on('change', function() {
        self.filter();
    });
    
    //checkboxes
    $('#checkboxDepletion').on('change', function() {
        self.filter();
    });
    
    $('#checkboxEnrichment').on('change', function() {
        self.filter();
    });
    
    $('#checkboxOnlyOverlap').on('change', function() {
        self.filter();
    });
    

};

Settings.prototype.filter = function() {
    
    this.counter = 0 ;
    
    this.data.sort(function(a, b) {
            return a.crosstalk.pValue.displayed - b.crosstalk.pValue.displayed ;
        });
        
    for (var i = 0,
        j = this.data.length; i < j; i++) {
        this.data[i].display = this.rules(this.data[i],this.counter);
        this.data[i].display ? ++this.counter : 0;
    };
 
    this.dispatcher.call('update');

};

Settings.prototype.rules = function(pathway,i) {
    
    //pvalue < 1
    if (pathway.crosstalk.pValue.displayed >  $('#pvalueNumberInput').val())
        return false;
        
    if (i+1 >  $('#' + this.sliderfilterID).val())
        return false;
    
    if (pathway.crosstalk.pValue.displayed == 1)
        return false;

    if ((pathway.crosstalk.type == 'depleted') & !$('#checkboxDepletion')[0].checked)
        return false;

    if ((pathway.overlap.size == 0) & $('#checkboxOnlyOverlap')[0].checked)
        return false;

    if ((pathway.crosstalk.type == 'enrichment') & !$('#checkboxEnrichment')[0].checked)
        return false;

	//console.log(pathway.crosstalk.pValue.displayed);
	//console.log(pathway.crosstalk.pValue[$('#correctionMethod').val()]);
	//console.log($('#correctionMethod').val());
    return true;
};

Settings.prototype.dispatcher = function() {
};


