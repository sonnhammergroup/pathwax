//PIE
function PieChart(parser,id){
    var self = this; 
    this.wait = false;
    self.parser = parser;
    self.data = self.parser.data;
    
    //self.colorPie = colorbrewer.RdYlBu[6];
    self.colorPie = colorbrewer.RdYlBu[11].concat(colorbrewer.RdYlBu[11]);
 	if(self.data.length < 8){
 		self.colorPie = colorbrewer.RdYlBu[8];
 	}
    self.pieDim = {w:250, h: 250};
    self.pieDim.r = (Math.min(self.pieDim.w, self.pieDim.h) / 2) - 2;
    self.svg = d3.select('#'+id).append("svg")
                        .attr("width", self.pieDim.w)
                        .attr("height", self.pieDim.h)
                        .append("g")
                         .attr("transform", "translate("+self.pieDim.w/2+","+self.pieDim.h/2+")");
   
   self.update();

}

PieChart.prototype.eventListener = function(){};

PieChart.prototype.update = function(){
    this.wait = true;
    
    var self = this;
   	self.data = self.parser.data;
    
    var arc = d3.arc().outerRadius(self.pieDim.r - 10).innerRadius(0);
    var pie = d3.pie().value(function(d) { return d.freq; }).sort(null);
    
    function arcTween(a) {  
            var i = d3.interpolateObject(this._current,a);
            this._current = i(0);
            return function(t) { return arc(i(t));    };
        }    
   
  // DATA JOIN
  // Join new data with old elements, if any. 
    var P = self.svg.selectAll("path").data(pie(self.data));

  // UPDATE
  // Update old elements as needed.
	P.transition().on('end',function(){self.wait = false;})
          .duration(500)
          .attrTween("d", arcTween);

  // ENTER + UPDATE
  // After merging the entered elements with the update selection,
  // apply operations to both.
     P.enter().append("path")
          .each(function (d) {
            this._current = {
              data: d.data,
              value: d.freq
            };
          })
          .attr("fill", function (d, i) {
            return self.colorPie[self.data.indexOf(d.data)];;
          })                             
          .on("mouseover",function(d){
                self.eventListener.call('mouseover',{},d.data.ID);
                })
          .on("mouseout",function(d){
                self.eventListener.call('mouseout',{},d.data.ID);
                })
          .transition().on('end',function(){self.wait = false;})
          .duration(500)
          .attrTween("d", arcTween)
           ; // redraw
// EXIT
  // Remove old elements as needed. 
      P.exit()
          .transition()
          .duration(500)
          .attrTween("d", function(d, index) {
	        var i = d3.interpolateObject(this._current,{startAngle: Math.PI * 2, endAngle: Math.PI * 2, value: 0});
	        return function(t) {return arc(i(t));}
	        ;
	} )
	 .remove()
	 ; 
    
        
        
};   


PieChart.prototype.mouseover = function(d){
	if(!this.wait){
    var arcOver = d3.arc().outerRadius(this.pieDim.r + 1).innerRadius(0);
    this.svg.selectAll("path").filter(function(p) {  return d == p.data.ID; }).transition()
           .duration(200)
           .attr("d", arcOver);
	}
};
PieChart.prototype.mouseout = function(d){
	if(!this.wait){
    var arc = d3.arc().outerRadius(this.pieDim.r - 10).innerRadius(0);

    this.svg.selectAll("path").filter(function(p) { 
         return (d == p.data.ID ); }).transition()
       .duration(200)
       .attr("d", arc);
    }   
};

//LEGEND
function Legend(parser,id){
    this.parser = parser;
    // create table for legend.
    this.table = d3.select('#'+id).append("table").attr('class','highlight blue-grey-text text-darken-2');
    
    this.header =  this.table.append("thead").append("tr");
        this.header.append('th').attr('data-field',"id").text("");
        this.header.append('th').attr('data-field',"q-value").text("Pathway class");
        this.header.append('th').attr('data-field',"genes").text("Significant pathways");

    
    this.legend = this.table.append("tbody");
    this.update();
};

Legend.prototype.eventListener = function(){};

Legend.prototype.update = function(){
    var self = this;
    var data = this.parser.data;
    
    
    var colorPie = colorbrewer.RdYlBu[11].concat(colorbrewer.RdYlBu[11]);
 	if(data.length < 8){
 		colorPie = colorbrewer.RdYlBu[8];
 	}
  	
  
    var rows = this.legend.selectAll("tr").data(data);

    rows.select(".legendrect").attr("width", '16').attr("height", '16').append("rect")
        .attr("width", '16').attr("height", '16')
        .attr("fill",function(d){ return colorPie[data.indexOf(d)]; });
    rows.select(".legendID").text(function(d){ return d.ID;}); 
    rows.select(".legendFreq").text(function(d){ return d.freq;}); 
    
    var new_row = rows.enter().append('tr')
                .on("click",function(d){self.selectTermClass(d);})
                .on("mouseover",function(d){
                    self.eventListener.call('mouseover',{},d.ID);
                    })
                .on("mouseout",function(d){
                    self.eventListener.call('mouseout',{},d.ID);
                    });
      
    new_row.append('td').attr("class",'legendrect').append("svg").attr("width", '16').attr("height", '16').append("rect")
        .attr("width", '16').attr("height", '16')
        .attr("fill",function(d){ return colorPie[data.indexOf(d)]; });
    new_row.append('td').attr("class",'legendID').text(function(d){return d.ID;});
    new_row.append('td').attr("class",'legendFreq').text(function(d){return d.freq;});
       
    rows.exit().remove(); 
    
    
    //reset selected terms
    this.legend.selectAll("tr").classed("fade", false);
    this.legend.selectAll("tr").classed("active", false);
};


Legend.prototype.selectTermClass = function(termClass){
   var active = this.legend.selectAll("tr").filter(function(p) {return (termClass == p);}).classed('active');
   
    this.legend.selectAll("tr").classed("fade", function (d) {          
                  return active ? false : (termClass != d);
              });
                
    this.legend.selectAll("tr").classed("active", function (d) {        
                  return active ? false : (termClass == d);
              });   
    
    active ? this.eventListener.call('classSelector',{},"") :  this.eventListener.call('classSelector',{},termClass);   
    
    
};

 

Legend.prototype.mouseover = function(d){
    this.legend.selectAll("tr").filter(function(p) {   return d == p.ID; }).style("background", "#f2f2f2");
};

Legend.prototype.mouseout = function(d){ 
    this.legend.selectAll("tr").filter(function(p) {   return d == p.ID ;}).style("background", "");
};



//HELPER
OverviewDataParser  = function(data_raw){
    this.data_raw = data_raw;   
    this.data = [];
    this.update(); 
};

OverviewDataParser.prototype.update = function(){
    
    var tmp = {};
    var classes = {ID:[]};
    this.data = [];
    
    for(var i=0,j=this.data_raw.length; i<j; i++){
      if(this.data_raw[i].display){
      	this.data_raw[i].pathway.termClass.forEach(function(value,key){
      		tmp[key] = ++tmp[key] || 1;
      		 }      );
      }   
    };
    
    for(var i=0,j=classes.ID.length; i<j; i++){
        if(Object.keys(tmp).indexOf(classes.ID[i]) < 0)
            tmp[classes[i]] = 0;
    };

    for(var i=0,j=Object.keys(tmp).length; i<j; i++){
            this.data.push({'ID':Object.keys(tmp)[i],
                        'freq':tmp[Object.keys(tmp)[i]]});
    }; 
    
    this.data.sort(function(a, b) { if (a.ID < b.ID)
				return -1;
			if (a.ID > b.ID)
				return 1;
			return 0;
		});
};


//HEATMAP
function HeatMap(data,id){

    this.tip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
                   return "<strong>" + d.gene.name + "</strong><span style='color:#f57c00'> " + d.raw + " links</span>";
          });
    
    this.table = d3.select('#'+id).append("table").attr('class','highlight blue-grey-text text-darken-2');
    
    this.table.append("col").attr('width','4%');
    this.table.append("col").attr('width','26%');
    this.table.append("col").attr('width','10%');
    this.table.append("col").attr('width','60%');
    
    //self.update(self.data);
    
    this.header =  this.table.append("thead").append("tr") ;
    this.header.append('th').attr('data-field',"nr").text("#");
    
    this.header_pathways = this.header.append('th').attr('data-field',"id");
    this.header_pathways.append('text').text("Pathway");
    this.header_pathways.append('text').attr('size',3).text(" (");
    this.header_pathways.append('font').attr('size',3).attr('color',"#2196f3").text("enriched");
    this.header_pathways.append('text').attr('size',3).text(" / ");
    this.header_pathways.append('font').attr('size',3).attr('color',"#ef5350").text("depleted ");
    this.header_pathways.append('text').attr('size',3).text(")");
    
    this.header.append('th').attr('data-field',"q-value").attr('id',"pValue").text("FWER");
    this.header_connections = this.header.append('th').attr('data-field',"genes");
    this.header_connections.append('text').text("Network connectivity of query genes"); 
    this.header_connections.append('text').attr('size',3).text(" (");
    this.header_connections.append('font').attr('color',"#756bb1").attr('size',3).text("pathway gene");
    this.header_connections.append('text').attr('size',3).text(")");
        
    this.table = this.table.append("tbody");
    
    
    
    this.maxwidth_links = (($( document ).width()/12) *10) * 0.6;

    this.elements_size = global.RESULT.network.bound.size;
    this.elements_order = global.RESULT.network.QueryIDArray;
    
    this.margin = {
        top : 10,
        right : 0,
        bottom : 20,
        left : 30,
    };
    
    
    this.grid = {
        width : Math.min(20, (this.maxwidth_links/this.elements_size)-4 ),    
        height : 22 
        };
    
    if(this.grid.width <2){
        this.grid.width = 2;
    }
        
    this.colors = {
        classes : colorbrewer.RdYlBu[6],
        overlap : colorbrewer.Purples[6] ,
        nooverlap : colorbrewer.Greens[6]
    };
   

    this.update(data);


};

HeatMap.prototype.eventListener = function(){ };

HeatMap.prototype.updateFDR = function(new_fdrData){
	// one could use the standard update function but the amount of objects getting validated causes lag in firefox. 
	//update header
	var correctionMethod = $('#correctionMethod').val();
	if(jQuery.isEmptyObject(correctionMethod))
		correctionMethod = 'fwer';
		
	if(correctionMethod == 'value'){
		$('#pValue').text('p-value( not corrected )');
	}else{
		$('#pValue').text('q-value ('+ correctionMethod.toUpperCase() +')');
	}
	
	// update values
	var rows = this.table.selectAll("tr").data(new_fdrData,function(d){return [d.pathway.name[0]];});      
    
    rows.select("#qValue")
                    .text(function(d){
                        return [d.crosstalk.pValue[correctionMethod].toExponential(4)]; 
                        });
       
};

HeatMap.prototype.update = function(new_data){
        
    var self = this;
    
 	var rows = self.table.selectAll("tr").data(new_data,function(d){return [d.pathway.name[0]];});
 
    var new_row = rows.enter().append('tr')
    			.on("click", function(d){self.eventListener.call('switchToDetails','',d.pathway.id);})
    			//.attr("style","cursor:pointer")
                .on("mouseover",function(d){
                	d.pathway.termClass.forEach(function(value,key){self.eventListener.call('mouseover','',key);});  
                	//self.eventListener.call('mouseover','',d.pathway.termClass);                 
                    })
                .on("mouseout",function(d){
                	d.pathway.termClass.forEach(function(value,key){self.eventListener.call('mouseout','',key);});   
                    //self.eventListener.call('mouseout','',d.pathway.termClass);
                    });; 

        
        new_row.append('td')
                    .attr("id","rank")
                        .text(function(d,i){return i+1;});
                        
        new_row.append('td')
                    .attr("id","Name_Link").append('a')
                    //.attr("href",function(d){return getDbLink(d.pathway.id);})
                    //.attr("style","cursor: help")
                    .attr("target","_blank")
                    .append("font")
                        .attr("color",function(d){
                                if(d.crosstalk.type=="enrichment")
                                    return '#2196f3';
                                if(d.crosstalk.type=='depleted')
                                    return '#ef5350';
                            })
                        .text(function(d){return [d.pathway.name[0]];});
        
        
        new_row.append('td')
                    .attr("id","qValue").text(function(d){
                        return [d.crosstalk.pValue.displayed.toExponential(2)];      
                        });
             
    var svg = new_row.append('td')
                        .append("svg").attr("height", '20')
                            .attr("width",   self.maxwidth_links)
                            .append("g")
                                .attr("class",'links')
                                .attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

    var links_chart = svg.selectAll(".rect").data(function(d){ 
                                                             return d.linksreal.map(function(v,i){
                                                                    tmp = d.overlap.has(self.elements_order[i]) ? true : false ;
                                                                    return {gene:d.query.get(self.elements_order[i]), 
                                                                            norm:(v / d.crosstalk.N),
                                                                            raw:v,
                                                                            overlap :tmp,
                                                                            pathway:d.pathway};
                                                                    });
                                                              
                                                              });
                                                              
    links_chart.enter()
            .append('rect')
                .attr("x", function(d, i) {return i * (self.grid.width+2);})
                .attr("rx", 2)
                .attr("ry", 2)
                .attr("width", self.grid.width)
                .attr("height", self.grid.height)
                .attr("transform", "translate(0,-" + self.grid.height / 2 + ")")
                .style("fill", function(d, i) {
                    return d.overlap ? self.colorScheme(d.norm,self.colors.overlap) : self.colorScheme(d.norm,self.colors.nooverlap);
                    })
                   .on('mouseover', self.tip.show)
                   .on('mouseout', self.tip.hide);
                   //.on("click", function(d) {  });
                   //.on("click", function(d) { window.open(FunCoupLink(d.gene.name,d.pathway)); });
       
   
   

    rows.exit().remove();
    svg.call(self.tip); 
    

};

HeatMap.prototype.colorScheme = function(d,colors){

    if(d==0)
        return '#fff';
    if(d > 0 & d <= 0.01){
        return colors[1];
    }
    if(d > 0.01 & d <= 0.02){
        return colors[2];
    }
    if(d > 0.02 & d <= 0.5){
        return colors[3];
    }
    if(d > 0.05 & d <= 0.1){
        return colors[4];
            }
    if(d > 0.1 & d <= 0.5){
        return colors[5];
            }
    if(d > 0.5 & d <= 1){
        return colors[6];
            }
};

HeatMap.prototype.validate = function(){
    this.table.selectAll("tr").each(function(d){d.display ?  $(this).show() : $(this).hide();}); 
};


HeatMap.prototype.selectTermClass = function(obj){
	//here the obj is a map not a string!
    if(obj == ""){
        this.validate();
    }else{
        this.table.selectAll("tr").each(function(d){
        	var show = false;
           d.pathway.termClass.forEach(function(value,key){
            if(d.display & obj.ID == key)
            	show = true;          	
           });
           show  ?  $(this).show():  $(this).hide();
            });
            } 
};


/*
 * HELPER FUNCTIONS
 */

//Sorting

function sortAlph(obj){
      obj.data.sort(function(a, b) {
			if (obj.lable(a) < obj.lable(b))
				return -1;
			if (obj.lable(a) > obj.lable(b))
				return 1;
			return 0;
		});
	obj.update();
}


function pathwaySize(obj){
     obj.data.sort(function(a, b) {
			return b.pathwaySize - a.pathwaySize;
		});
	obj.update();
}


function p_Value(obj){
     obj.data.sort(function(a, b) {
			return a.crosstalk.pValue.displayed - b.crosstalk.pValue.displayed ;
		});
	obj.update();
}

function degreeGene(obj){
		obj.data.sort(function(a, b) {
			return sumArray(a.linksreal) - sumArray(b.linksreal);
		});
			obj.update();
}

function BH_adjust(obj){
        
     obj.update();
}

function rangeSlider(obj) {
    
	obj.data = obj.data_all.filter(function(d) {
		return d.crosstalk.pValue.displayed <= document.getElementById('threshold1').value;
	});
	
	//if nothing left find smallest one
	
	if(obj.data.length == 0){
	    var idx = 0;
	    for(var i=0,j=obj.data_all.length; i<j; i++){
		   if(obj.data_all[i].crosstalk.pValue.displayed < obj.data_all[idx].crosstalk.pValue.displayed)
		      idx = i;
		};
		obj.data = [obj.data_all[idx]];
	}
	
	obj.update();
}

function changeToFDR(obj) {
	
for(var i=0,j=obj.data_all.length; i<j; i++){
		  obj.data_all[i].crosstalk.pValue.displayed = obj.data_all[idx].crosstalk.pValue.fdr;
		 };
		 
obj.update();

};



/*
function changeToClassName(obj) {
	
obj.lable = function(obj){
	return obj.pathway.termClass;
};

obj.update();
};


function changeToSubClassName(obj) {
	
obj.lable = function(obj){
	return obj.pathway.termSubClass;
};

obj.update();
};
function changeToPathwayName(obj) {
obj.lable = function(obj){
	return obj.pathway.name[0];
};

obj.update();
}

*/



