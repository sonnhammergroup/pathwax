
ExportStatsToCsv = function (filename,data) {
		var row = [];
		row.push(['Pathway','PathwaySize','FWER','pValue','CrossTalk','type']);
		for(var i=0,j=data.length; i<j; i++){
		  if(data[i].display){
		  	var tmp = [];
		  	tmp.push(data[i].pathway.name);
		  	tmp.push(data[i].crosstalk.pValue.corrected.toPrecision(3));
		  	tmp.push(data[i].crosstalk.pValue.value.toPrecision(3));
		  	tmp.push(data[i].crosstalk.k);
		  	tmp.push(data[i].crosstalk.type);
		  	row.push(tmp);
		  };
		};
	

        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';
        };

        var csvFile = '';
        for (var i = 0; i < row.length; i++) {
            csvFile += processRow(row[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    };

ExportGraphToCsv = function (filename,data) {
		var row = [];
		row.push(['GeneA','GeneB']);
		
		//get identifier
		var value = $("#nodeLableID").val(); 
		if(value == 'Ensembl' || value == 'degree' || value == 'none'){
			console.log('default')	;
			for(var z=0,y=data.links.length; z<y; z++){
			  var tmp = [];
			  tmp.push(data.links[z].source.gene.name);
			  tmp.push(data.links[z].target.gene.name);
			  row.push(tmp);
			};
		}else{
		 console.log('default');
		 		for(var z=0,y=data.links.length; z<y; z++){
		  var tmp = [];
		  tmp.push(data.links[z].source.gene.alternativeID.get(value));
		  tmp.push(data.links[z].target.gene.alternativeID.get(value));
		  row.push(tmp);
		};	
		}
		


        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';
        };

        var csvFile = '';
        for (var i = 0; i < row.length; i++) {
            csvFile += processRow(row[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    };

