
//evaluate parameters
function calc(n, p, k) {

	var mx = n * p;
	var varx = mx * (1 - p);
	
	var result = jStat.binomial.pdf(k,n,p);
	if(!isNaN(result)){      
        return {value:result,method:'binomial'};
    }else{
	//use Binomial dist
    	if (n < 1001) {
    		return {value:binom(n, p, k),method:'binomial'};  		
    	}
        if (mx >= 5 && n * (1- p) >= 5) {  
            return {value:norm(n, p, k),method:'normal'};
            
        }
    	if (n > 149 && varx >= .9 * mx) {
        	    return {value:jStat.poisson.pdf( k, n*p),method:'poisson'};
    	//	return {value:pois(n, p, k),method:'poisson'};
    	}
        return NaN;
    }
    

}



//calc binomial dist
function binom(n, p, r) {
	var tabz = [];
	var a = 1 * 0;
	var b = 1 * 0;
	var sum = 1 * 0;
	var prob = 1 * 0;
	var nr = 1 * 0;
	var pValue = 1 * 0;
	var q = 1 - p;
	var factn = 0;
	var factr = 0;
	var factnr = 0;

	for (var i = 1,
	factn = 0; i < n; i++, factn += Math.log(i));

	for (var j = 0; j < n + 1; j++) {

		for (var  i = 1,
		factr = 0; i < j; i++, factr += Math.log(i));

		nr = n - j;
		for ( i = 1,
		factnr = 0; i < nr; i++, factnr += Math.log(i));

		a = factn - (factr + factnr);

		a = Math.exp(a);
		b = Math.pow(p, j) * Math.pow(q, n - j);
		tabz.push(a * b);
	}

	if (r >= n * p) {
        
		for (var j = r; j < n + 1; j++) {
			pValue += tabz[j] * 1;
		}

		return pValue;
	}
	return NaN;
}

function pois(n, p, r) {
	var sum = 1 * 0;
	var sumx = 1 * 0;
	var e = Math.E;
	var mx = n * p;
	var factr = 0;


	for (var  i = 1, factr = 1; i < r; i++, factr *= i);

    
    console.log( (Math.pow(e, -mx) * Math.pow(mx, r)) + ' ' + (Math.pow(e, -mx) * Math.pow(mx, j)));
	sumx = (Math.pow(e, -mx) * Math.pow(mx, r)) / factr;

	for (var j = 0; j < r + 1; j++) {
		for (var  i = 1, factr = 1; i < j; i++, factr *= i);
		  sum += (Math.pow(e, -mx) * Math.pow(mx, j)) / factr;
	}

	var pValue = 1 - sum + sumx;

	if (r == 0) {
		return 1;
	};

	if (pValue >= 0) {
		return pValue;
	} 
	
	if (pValue >= -1) {
        return 0;
    } else {
		return NaN;
	};

}

//calc normal dist
function norm(n, p, r) {

	var sdx = 1 * 0;
	var mx = n * p;
	var varx = mx * (1 - p);
	
	sdx = Math.sqrt(varx);
	sdx = rnd4(sdx);

	var diff = r - mx;
	var z = 1 * 0;

	if (r > mx) {
		z = (diff - .5) / sdx;
		z = Math.round(100 * z) / 100;
	}

	if (r < mx) {
		z = (1 * diff + .5) / sdx;
		z = Math.round(100 * z) / 100;
	}

	if (r == mx) {
		z = 1 * 0;
	}

	z = Math.abs(z);

	var p2 = (((((.000005383 * z + .0000488906) * z + .0000380036) * z + .0032776263) * z + .0211410061) * z + .049867347) * z + 1;
	p2 = Math.pow(p2, -16);

	var p1 = p2 / 2;

	var otr = 1 * 0;
	z = z + (1 / sdx) * 1;

	var p2x = (((((.000005383 * z + .0000488906) * z + .0000380036) * z + .0032776263) * z + .0211410061) * z + .049867347) * z + 1;
	p2x = Math.pow(p2x, -16);
	otr = p2 - p2x;
	otr = otr / 2;

	if (r > mx) {
		return p1;
	}

	if (r < mx) {
		return 1 - p1 + otr;
	}

	if (r == mx) {
		p1 = .5 + (otr / 2);
		p2 = "1.0";
		return p1;
	}
	
}

//round 4 dec
function rnd4(x) {
	return Math.round(x * 10000) / 10000;
}

//round 6 dec
function rnd6(x) {
	if (x >= .000001) {
		return Math.round(x * 1000000) / 1000000;
	} else {
		return "<0.000001";
	}
}

//round 12 dec
function rnd12(x) {
	if (x >= .000000000001) {
		return Math.round(x * 1000000000000) / 1000000000000;
	} else {
		return "<0.000000000001";
	}
}
