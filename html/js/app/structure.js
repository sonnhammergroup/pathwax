/* ------------------------------------- */

function Base() {
};

Base.prototype._status = false;

Base.prototype.setID = function(obj) {
	this.id = obj;
};

Base.prototype.setName = function(obj) {
	this.name = obj;
};
Base.prototype.setDesc = function(obj) {
	this.desc = obj;
};
Base.prototype.setUrl = function(obj) {
	this.url = obj;
};

/* ------------------------------------- */

Organism.prototype = new Base();
Organism.prototype.constructor = Organism;
function Organism() {
};

Organism.prototype.setTaxID = function(obj) {
	this.taxID = obj;
};
Organism.prototype.setShortName = function(obj) {
	this.shortName = obj;
};
Organism.prototype.setOther = function(obj) {
    this.other = obj;
};
Organism.prototype.setExample = function(obj) {
    this.example = obj;
};
Organism.prototype.setTerms = function(obj) {
	this.terms = obj;
};
Organism.prototype.setIMG = function(obj) {
	this.img = obj;
};
Organism.prototype.setFcGenomeID = function(obj) {
    this.fcGenomeID = obj;
};

Organism.prototype.setIdentifierSource = function(obj) {
    this.identifierSource = obj;
};

/* ------------------------------------- */
Gene.prototype = new Base();
Gene.prototype.constructor = Gene;

function Gene() {
	this.alternativeID = new Map();
};

Gene.prototype.setEnsemblID = function(obj) {
	this.ensemblID = obj;
};

Gene.prototype.setSpecies = function(obj) {
	this.species = obj;
};


/* ------------------------------------- */
Pathway.prototype = new Base();
Pathway.prototype.constructor = Pathway;

function Pathway() {
	this.genes = new Map();
	this.randomLinks = null;
};

Pathway.prototype.setClass= function(obj) {
	this.termClass = new Map();
	
	var tmp_class = Object.keys(obj).toString().split(",");
	var tmp_SubClass = Object.keys(obj).toString().split(",");
		
	for(var i=0,j=tmp_class.length; i<j; i++){		
	  	this.termClass.set(tmp_class[i],tmp_SubClass[i]);
	};
};

Pathway.prototype.addGenes = function(obj) {
	this.genes.set(obj.id,obj);
};

Pathway.prototype.setOutDegree = function(obj) {
	this.outdegree = obj;
};


Pathway.prototype.setRandomLinks = function(obj) {
    this.randomLinks = obj;   
};

Pathway.prototype.setTerm = function(obj) {
	this.term = obj;
};

Pathway.prototype.validate = function() {
    this._status = (this.randomLinks >= 0) ? true : false;
};

Pathway.prototype.setGraph = function(obj) {
	this.graph = obj;
};

/* ------------------------------------- */

Term.prototype = new Base();
Term.prototype.constructor = Term;
function Term() {
	this.pathways = [];
};
/* ------------------------------------- */
/* ------------------------------------- */


Container.prototype = new Map();
Container.prototype.constructor = Container;
function Container() {};


function Statistics() {
	this.crosstalk = new CrossTalk();
    this.display = true;
};

Statistics.prototype._status = false;

Statistics.prototype.setPathway = function(obj) {
	this.pathway = obj;
};

Statistics.prototype.setLinkArrayReal = function(obj) {
	this.linksreal = obj;
};

Statistics.prototype.setLinkArrayRandom = function(obj) {
	this.linksrandom = obj;
};

Statistics.prototype.setPathwaySize = function(obj) {
	this.pathwaySize = obj;
};

Statistics.prototype.setQuery = function(obj) {
    this.query = obj;
};

Statistics.prototype.setOutDegreeSignature = function(obj) {
	this.outdegreeSignature = obj;
};

Statistics.prototype.setOutDegreePathway = function(obj) {
    this.outdegreePathway = obj;
};

Statistics.prototype.setOverlap = function(obj) {
    this.overlap = obj;
};

Statistics.prototype.getCrossTalk = function() {

	this.crosstalk.setN(Math.min(this.outdegreePathway,this.outdegreeSignature, this.pathwaySize * this.query.size));

	this.crosstalk.setk(sumArray(this.linksreal));

	this.crosstalk.setIterations(1000);


	this.crosstalk.setmean(this.linksrandom);

	this.crosstalk.calc();

};

function CrossTalk() {
};

CrossTalk.prototype._status = false;

CrossTalk.prototype.setN = function(obj) {
	this.N = obj;
};

CrossTalk.prototype.setk = function(obj) {
	this.k = obj;
};

CrossTalk.prototype.setIterations = function(obj) {
	this.iterations = obj;
};

CrossTalk.prototype.setp = function(obj) {
	this.p = this.mean / this.N;
};

CrossTalk.prototype.setmean = function(obj) {
	this.mean = obj / this.iterations;
};

CrossTalk.prototype.method = function(obj) {
	this.method = obj;
};

CrossTalk.prototype.calc = function(obj) {
	if (this.N <= 0)
		throw Error("N <= 0");

	if (!Boolean(this.iterations))
		throw Error("define iterations");

	this.setp();

	if (this.mean < this.k) {
		this.pValue  = calc(this.N, this.p, this.k-1);
		this.type = "enrichment";
		this._status = true;
	}
    else{
        this.pValue  = calc(this.N, this.p, this.k);
        this.type = "depleted";
        this._status = true;
    }
};

/* ------------------------------------- */
function Node(gene,pathway) {
    this.group = pathway;
	this.id = gene;
};

Node.prototype._status = false;


function Link(source,target,value) {
    this.source = source;
	this.target = tarrget;
	this.value = value;
};

Link.prototype._status = false;


function Graph(nodes,links) {
	this.nodes = nodes;
    this.links = links;
};

Graph.prototype._status = false;

