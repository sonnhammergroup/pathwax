/* ****************** ***********************
 *           DATABASE REQUEST
 ******************** ******************** */

function DataBaseCall() {
	this.url = "/cgi-bin/rpc_json.py";
}

DataBaseCall.prototype.setMethod = function(obj) {
	this.dbcall = obj;
};

DataBaseCall.prototype.setRequest = function(obj) {
	this.request = obj;
};

DataBaseCall.prototype.setSpecies = function(obj) {
	this.speciesID = obj;
};

DataBaseCall.prototype.clear = function() {
	delete this.dbcall, this.request, this.speciesID, this.req;
};

DataBaseCall.prototype.call = function() {

	var self = this;

	self.id = Math.floor((Math.random() * 100000) + 1);

	var req = 'jsonrpc=2.0' + '&method=' + self.dbcall + '&id=' + self.id;

	req = req + "&species=" + self.speciesID + "&query=" + self.request;

	//FOR DEBUGING:
	//console.log('localhost:8000/'+self.url+'?'+req);

	self.rpc = d3.json(self.url, function(error, json) {
		if (error)
			return console.warn(error);

		//FOR DEBUGING:		console.log(json.result);
		self.update(json.result);
		self.clear();
	});
	
	self.rpc.header("Content-type", "application/x-www-form-urlencoded").send("POST", req);
    
};

DataBase.prototype.update = function() {};

/* ------------------------------------- */
function DataBase() {
	this.species = [];
};
DataBase.prototype._status = false;
DataBase.prototype.load = function() {
	var self = this;

	var dataBaseCall = new DataBaseCall();
	dataBaseCall.setMethod('species');
	dataBaseCall.setSpecies('');
	dataBaseCall.setRequest('');

	dataBaseCall.update = function(data) {
		for (var i in data) {
			var elem = new Organism();
			elem.setID(data[i]['fcID']);
			elem.setName(data[i]['long']);
			elem.setTaxID(data[i]['taxID']);
			elem.setShortName(data[i]['short']);
            elem.setOther(data[i]['other']);
			elem.setTerms(data[i]['terms']);
            elem.setFcGenomeID(data[i]['fcgenomeID']);
            elem.setIdentifierSource(data[i]['identifierSource']);

            elem.setExample(data[i]['example']);


			self.species.push(elem);
		}

		if (self.species.length > 0) {
			self._status = true;
		} else {
			//TODO throw error if no species in DB
		}

		self.clear();
		self.updateInterface();
	};

	dataBaseCall.call();
};

DataBase.prototype.find = function(id) {
    for(var i=0,j=this.species.length; i<j; i++){
      if(this.species[i].id[0] == id)
        return this.species[i];
    };
    return null;
};

DataBase.prototype.clear = function() {
	delete dataBaseCall, self;
};

DataBase.prototype.updateInterface = function() {
};

/* ****************** ***********************
 *     			   INSTANCE
 ******************** ******************** */

function Instance(obj) {
	var self = this;
	if ( obj instanceof Organism) {

		this.species = obj;

		//define query
		this.query = new Query();
		this.query.setSpecies(this.species);
		this.query.updateStatus = function() {
			self.call('query');
		};

		//define network and assign query and pathway set
		this.network = new Network();
		this.network.setSpecies(this.species);
		this.network.setQuery(this.query);
		this.network.updateStatus = function() {
			self.call('network');
		};

		//define set containing terms
		this.termset = new TermSet();
		this.termset.setSpecies(this.species);

		//define terms
		var inputTerms = this.species.terms;
	
		var tmp = new Term();
		tmp.setID(inputTerms[$('#termsetSelection').val()]);
		this.termset.addTerm(tmp);

		this.termset.setGeneSet(this.network.adjacent);
        this.termset.setBoundQuery(this.network.bound);
		this.termset.updateStatus = function() {
			self.call('termset');
		};

		//define result, for every term in termset a new analysis
		this.result = new Analyse();
		this.result.setQuery(this.query);
		this.result.setNetwork(this.network);
		this.result.setTermSet(this.termset);		
		this.result.updateStatus = function() {
            self.call('analysis');
        };
        
        this.translateToEnsemble = new TranslateToEnsembl();
        this.translateToEnsemble.updateStatus =  function() {
            self.updateStatus();
        };

	} else {
        throw Error("selected species not found");
	}

};

Instance.prototype.setQuery = function(obj) {
	this.query.setInput(obj);
};

Instance.prototype.setTermSet = function(obj) {
	this.pathways.setTerms(obj);
};

Instance.prototype.call = function(obj) {
	//if species is missing
	if (!Boolean(this.species)) {
		throw Error("no species defined");
	};

	/*
	* 1.query
	* 2a network adjacent nodes
	* 2b network links and random links
	* 3 all pathway terms
	*
	*/

	//console.log("-----NEW CALL------");
    //console.log(global);
    //console.log(obj)
    
	if (Boolean(this.query.input)) {
		if (!this.query._status) {
           // console.log("-----Query------");
            $('#query_status').text("translating genes");
			this.query.translate();
		}
	}


	if (this.query._status) {
		if (this.network.adjacent.size == 0) {
           // console.log("-----Network------");
            $('#query_status').text("generating subnetwork");
			this.network.loadAdjacent();		
		}
	}

	if (this.network.adjacent.size != 0) {
		if (!this.termset._status & !this.termset._active) {
            //console.log("------TermSet-----");
           // t0 = performance.now();
            $('#query_status').text("determining functionally coupled genes...this may take a moment");
           // TranslateToEnsembl(global.GENESET);
			this.termset.load();
			
		}
	}

	if (this.termset._status & this.network._status) {
		if (!this.result._status) {
            //console.log("-----Result------");
            $('#query_status').text("loading gene data of "+global.GENESET.size+" genes");
			this.result.init();
		}
	}
	
        
	if(this.result._status & !this.translateToEnsemble._status){
	       //console.log("------translate-----");
            $('#query_status').text("render results");
           this.translateToEnsemble.init(global.GENESET);
	}
 /*
	 console.log("Query :" + this.query._status + ",translated " + this.query.translated.length);
	 console.log("Network :" + this.network.adjacent._status + " neighbours:" + this.network.adjacent.length + " links:" + this.network.links.length + " rndlinks:" + this.network.randomlinks.length);
	 console.log("TermSet :" + this.termset._status+ " termsets:" + this.termset.terms.length);
	 console.log("Result :" + this.result._status + " results:" + this.result.length);
*/
};

Instance.prototype.updateStatus = function(){};


/* ****************** ***********************
 *     			   QUERY
 ******************** ******************** */
function Query() {
	this.translated = new Map();
	this.failedset = new Map();
};

//need at least one translated gene to get true
Query.prototype._status = false;

Query.prototype.setInput = function(obj) {
	this.input = obj;
};

Query.prototype.setSpecies = function(obj) {
	this.species = obj;
};

Query.prototype.translate = function() {
	var self = this;

	var dataBaseCall = new DataBaseCall();
	dataBaseCall.setMethod('translate');

	dataBaseCall.setRequest(this.input);
	dataBaseCall.setSpecies(this.species.taxID);

	dataBaseCall.update = function(data) {

		for (var i in data) {
			var elem = new Gene();

			elem.setID(parseInt(data[i]['internalGeneID']));
			elem.setName(data[i]['Keywords']);
			elem.setEnsemblID(data[i]['geneID']);
			elem.setSpecies(self.species);

			if (Boolean(elem.id)) {
				elem._status = true;
				self.translated.set(elem.id,elem);
                global.GENESET.set(elem.id,elem);
			} else {
				elem._status = false;
				self.failedset.set(elem);
			}
			

		}

		if (self.translated.size > 0) {
			self._status = true;
		} else {
			self._status = false;
            $('#modal_translation').openModal();
            
			throw Error("query translation request failed");
		}

		self.updateStatus();
	};
	dataBaseCall.call();
};

Query.prototype.updateStatus = function() {
};

/* ****************** ***********************
 *     			   NETWORK
 ******************** ******************** */
function Network() {
	this.adjacent = new Map();
	this.bound = new Map();
	this.free = new Map();
	
	
    this.QueryIDArray = [];
	this.links = new Map();

};

Network.prototype._status = false;

Network.prototype.load = function() {
	if (this.adjacent.size == 0) {
		this.loadAdjacent();
	}
/*
	if (this.adjacent._status) {
		TranslateToEnsembl(this.adjacent);
		this.loadLinks();
	}
	*/
};

Network.prototype.setQuery = function(obj) {
	this.query = obj;
};
Network.prototype.setSpecies = function(obj) {
	this.species = obj;
};
Network.prototype.setPathways = function(obj) {
	this.pathways = obj;
};


Network.prototype.getLinks = function(obj) {
    var self = this;
       
    if ( obj instanceof Gene) {
        return self.links.get(obj.id);
    };
    
    if ( obj instanceof Map) {
           
           var tmp = Array.apply(null, Array(self.QueryIDArray.length) ).map(Number.prototype.valueOf, 0);         
            obj.forEach(function(value, gene) {
                    var tmp_link = self.links.get(gene);
                    
                    if(Boolean(tmp_link)){
                    for (var n = 0, t = tmp_link.length; n < t; n++) {
                            tmp[n] += tmp_link[n];
                        }  
                      }          
                    }
                    ,tmp  );    
            
            return {
                real : tmp
            };
           
        
    };
   
};

Network.prototype.loadAdjacent = function() {
	var self = this;

	var IDSet = [];
	self.query.translated.forEach(function(value, key) {
              IDSet.push(key);
            },IDSet);

	var dataBaseCall = new DataBaseCall();
	dataBaseCall.setMethod('neighbour');
	dataBaseCall.setRequest(IDSet);
	dataBaseCall.setSpecies(self.species.id);

	dataBaseCall.update = function(data) {
		for (var i in data.boundNode) {
			self.bound.set(data.boundNode[i],self.query.translated.get(data.boundNode[i]));
		}
		
		for (var i in data.freeNode) {
            self.free.set(data.freeNode[i],self.query.translated.get(data.freeNode[i]));
		}
		
		for (var i in data.adjacent) {
		    var elem = global.GENESET.get(data.adjacent[i]);
		    //if gene does not exist
		    if(!Boolean(elem)){               
                var elem = new Gene();
                elem.setID(data.adjacent[i]);
                elem.setSpecies(self.species);
                global.GENESET.set(elem.id,elem);
		    }
            self.adjacent.set(elem.id,elem);    	
		}

    if(self.bound.size != 0){
            self.updateStatus();
            self.loadLinks();	        
	    }else {
            $('#modal_no_links_found').openModal();
            throw Error("no links found");
        }
		
	};
	
	dataBaseCall.call();
	IDSet = [];
};


Network.prototype.loadLinks = function() {
    var self = this;

    self.bound.forEach(function(value, key) {
              self.QueryIDArray.push(key);
            },self.QueryIDArray);
    
    var NeighbourIDArray = [];
    self.adjacent.forEach(function(value, key) {
              NeighbourIDArray.push(key);
            },NeighbourIDArray);


    var dataBaseCall = new DataBaseCall();
    dataBaseCall.setMethod('links');
    dataBaseCall.setRequest(self.QueryIDArray + '&neighbour=' + NeighbourIDArray);
    dataBaseCall.setSpecies(self.species.id);
    dataBaseCall.update = function(data) {
        //console.log(data)
        for (var i in data.real) {
            self.links.set(NeighbourIDArray[i],data.real[i]);
        }
        
        if (self.links.size > 0) {
            self._status = true;
            self.updateStatus();
        } else {
            throw Error("no links found");
        }
        
        

    };
    dataBaseCall.call();
};

Network.prototype.updateStatus = function() {
};

/* ****************** ***********************
 *     			   TERM
 ******************** ******************** */

function TermSet() {
	this.terms = [];

};

TermSet.prototype._status = false;
TermSet.prototype._active = false;

TermSet.prototype.addTerm = function(obj) {
	this.terms.push(obj);
};

TermSet.prototype.setGeneSet = function(obj) {
	this.geneset = obj;
};

TermSet.prototype.setBoundQuery = function(obj) {
    this.boundQuery = obj;
};

TermSet.prototype.setSpecies = function(obj) {
	this.species = obj;
};

TermSet.prototype.load = function() {
        
	var self = this;

    self._active = true;
    
	var qeryIDSet = [];
	//self.geneset.getIDArray(qeryIDSet);
	   self.geneset.forEach(function(value, key) {
              qeryIDSet.push(key);
            },qeryIDSet);
	
	
    var boundQueryIDSet = [];
    //self.boundQuery.getIDArray(boundQueryIDSet);
    self.boundQuery.forEach(function(value, key) {
              boundQueryIDSet.push(key);
            },boundQueryIDSet);

	for (var t = 0,
	    len = self.terms.length; t < len; t++) {

		var dataBaseCall = new DataBaseCall();
		
		dataBaseCall.setMethod('path');

		dataBaseCall.setRequest(qeryIDSet +'&bound=' + boundQueryIDSet + '&type=' + self.terms[t].id);

		dataBaseCall.setSpecies(self.species.id);

		//Bit crazy since the update function is called when the for loop progressed further
		dataBaseCall.term = self.terms[t];
		dataBaseCall.update = function(data) {
		    
		    
           // console.log(this.term.id);

			for (var i in data[this.term.id]) {
				//console.log(data[this.term.id][i]['CLASS']);	
				var elem = new Pathway();
				elem.setID(i);
				elem.setName(data[this.term.id][i]['NAME']);
				elem.setDesc(data[this.term.id][i]['DESCRIPTION']);
				elem.setClass(data[this.term.id][i]['CLASS']);	
				elem.setOutDegree(data[this.term.id][i]['FC4']);
				elem.setTerm(this.term);
				
				
				//console.log(elem);
				
				for (var m in data[this.term.id][i]['GENE']) {			    
				    var elemGene = global.GENESET.get(parseInt(data[this.term.id][i]['GENE'][m]));
                    //if gene does not exist
                    if(!Boolean(elemGene)){               
                        var elemGene = new Gene();
                        elemGene.setID(parseInt(data[this.term.id][i]['GENE'][m]));
                        elemGene.setSpecies(self.species);
                        global.GENESET.set(elemGene.id,elemGene);
                    }                 
				    elem.addGenes(elemGene);
				}
				
				this.term.pathways.push(elem);
			}

            
            $('#query_status').text("connected pathways");
            $('#pathway_nr').html(this.term.pathways.length);
            for (var i in this.term.pathways) {
                sumRandomLinks(self,this.term.pathways[i]);
            }
     

			if (this.term.pathways.length == 0) {
                $('#modal_no_pathway').openModal();
                    throw Error("no links found");
			}

			
          //  self._status = true;
            //self._active = false;
            
           //   t2 = performance.now();
          //  console.log("web parser: " + (t2 - t1)/100);
		 //self.updateStatus();

		};


		dataBaseCall.call();

	};

};

TermSet.prototype.validate = function(){
 /*  TOO ADVANCED FOR SAFARI 
   if(this.terms.every(elem => elem._status == true)){
            this._status = true;
            this._active = false;        
        }
        
    for (var t = 0,len = this.terms.length; t < len; t++) {        
        if(!this.terms[t]._status){
            if(this.terms[t].pathways.every(elem => elem.status == true)){
                this._status = true;
                this.terms[t]._status = true;
                this.updateStatus();
            };
        }          
         
    }
    */
   
      if(this.terms.every(testing_status)){
            this._status = true;
            this._active = false;        
        }
        
    for (var t = 0,len = this.terms.length; t < len; t++) {        
        if(!this.terms[t]._status){
            if(this.terms[t].pathways.every(testing_status)){
                this._status = true;
                this.terms[t]._status = true;
                this.updateStatus();
            };
        }          
         
    }
};

function testing_status(value, index, ar){
    if(value._status){
        return true;
    }
    else{ 
        return false;
    }
};
 


TermSet.prototype.updateStatus = function() {
};

/* ****************** ***********************
 *     			   ANALYSIS
 ******************** ******************** */

Analyse.prototype = [];
Analyse.prototype.constructor = Analyse;

function Analyse() {
	this.processingTerms = [];
    this.queryDegree = 0;
};

Analyse.prototype._status = false;

Analyse.prototype.setQuery = function(obj) {
	this.query = obj;
};

Analyse.prototype.setTermSet = function(obj) {
	this.termset = obj;
    this.terms_analysis_valid = [];
	for(var i=0,j=this.termset.terms.length; i<j; i++){
	   this.terms_analysis_valid.push(false);
	};
};

Analyse.prototype.setNetwork = function(obj) {
	this.network = obj;
};

Analyse.prototype.add = function(obj) {
	if ( obj instanceof Statistics) {
		this.push(obj);
	}
};

Analyse.prototype.has = function(obj) {
	if ( obj instanceof Statistics) {
		for (var i = 0; i < this.length; i++) {
			if (this[i].pathway.id == obj.pathway.id) {
				if (this[i].pathway.term.id == obj.pathway.term.id) {
					return Boolean(1);
				}
			}
		}
		return Boolean(0);
	}
};

Analyse.prototype.init = function() {
    var self = this;


    if(this.queryDegree==0){
    
    this.network.links.forEach(
        function(value,key){
           self.queryDegree += sumArray(value);
        }
        
    ,self.queryDegree) ;
    
    }
  
   /* 
    for(var i=0,j=this.network.links.length; i<j; i++){
        this.queryDegree = this.queryDegree + sumArray(this.network.links[i]);
        
    };
     */   
	//check if termset is ready for processing
	for (var i = 0,
	    j = this.termset.terms.length; i < j; i++) {

		if (this.termset.terms[i]._status &
		//if termset is valid
		(this.processingTerms.indexOf(this.termset.terms[i].id) < 0) &
		// if term is not in pipeline already
		(this.termset.terms[i].pathways.length > 0))
		// if pathways got already assign to object. seems like java script is only updating the pointers when calling a method
		{
		    //console.log(this.termset.terms[i]);
			this.processingTerms.push(this.termset.terms[i].id);

			for (var n = 0,
			    t = this.termset.terms[i].pathways.length; n < t; n++) {

				var elem = new Statistics();
				elem.setPathway(this.termset.terms[i].pathways[n]);

				if (!this.has(elem)) {


					elem.setOutDegreePathway(this.termset.terms[i].pathways[n].outdegree);
                    elem.setOutDegreeSignature(this.queryDegree);

					var tmp_links = this.network.getLinks(elem.pathway.genes);
					//for performance reasons do it at once

					elem.setLinkArrayReal(tmp_links.real);
					elem.setLinkArrayRandom(elem.pathway.randomLinks);
					elem.setPathwaySize(elem.pathway.genes.size);
					elem.setQuery(this.network.bound);
					elem.getCrossTalk();
                     
                        
                    //get overlap:
                    var overlap = new Set();
                    
                    elem.pathway.genes.forEach(function(value,key){
                        if(self.network.bound.has(key)){
                            overlap.add(key);
                        }
                        
                    },overlap);
                    
                    /*
                    for(var g=0,s=elem.pathway.genes.length; g<s; g++){
                      if(this.network.bound.has(elem.pathway.genes[g])){
                        overlap.add(elem.pathway.genes[g]);
                        }
                    };   
                    */
                    elem.setOverlap(overlap);
                    
					this.add(elem);
				} else {
					delete elem;
				}

			};
			this.terms_analysis_valid[i] = true;
		}
	};
	
	
	//only execute when all results are calculated
	if(this.terms_analysis_valid.indexOf(false) < 0){
        var self = this;
        self.sort(function(a, b) {
            return a.crosstalk.pValue.value - b.crosstalk.pValue.value;
        }); 
        
       // BH
        $.each(self, function( i ,d) {
        	//BH FDR correction
            //d.crosstalk.pValue['displayed'] = d.crosstalk.pValue.value * ( self.length - i);
            //FWER correction
            //d.crosstalk.pValue['corrected'] = d.crosstalk.pValue.value * self.length;
            //if(d.crosstalk.pValue['corrected'] > 1){
            //    d.crosstalk.pValue['corrected'] = 1;
            //}
            //BH FDR correction
            d.crosstalk.pValue['fdr'] = d.crosstalk.pValue.value * ( self.length - i);
            if(d.crosstalk.pValue['fdr'] > 1){
                d.crosstalk.pValue['fdr'] = 1;
            }
            
            //FWER correction
            d.crosstalk.pValue['fwer'] = d.crosstalk.pValue.value * self.length;
            
            if(d.crosstalk.pValue['fwer'] > 1){
                d.crosstalk.pValue['fwer'] = 1;
            }
            
            d.crosstalk.pValue['displayed'] = d.crosstalk.pValue['fwer'];

        });
        /*
         
         
       //Bonferroni
         $.each(self, function( i ,d) {
            d.crosstalk.pValue['corrected'] = d.crosstalk.pValue.value * self.length;
            if(d.crosstalk.pValue['corrected'] > 1){
                d.crosstalk.pValue['corrected'] = 1;
            }

        });
        */
       
               self.sort(function(a, b) {
            return a.crosstalk.pValue.corrected - b.crosstalk.pValue.corrected;
        }); 
        
        this._status = true;
        
	    this.updateStatus();
	}


    
};

Analyse.prototype.updateStatus = function() {};

/* ------------------------------------- */
function TranslateToEnsembl() {
    this._status = false;
};

TranslateToEnsembl.prototype.init = function(geneset){
    if ( geneset instanceof Map) {
        this._status = true;
        var self = this;
        self.geneset = geneset;

        var queryIDSet = [];
            self.geneset.forEach(function(value, key) {
                  value._status ? null : queryIDSet.push(key);
                },queryIDSet); 


        if (queryIDSet.length > 0) {
            
            var dataBaseCall = new DataBaseCall();
            dataBaseCall.setMethod('internalToEnsembl');
            dataBaseCall.setSpecies(self.geneset.get(queryIDSet[0]).species.id);
            dataBaseCall.setRequest(queryIDSet);

            dataBaseCall.update = function(data) {
                for (var i in data) {
                    var tmp = self.geneset.get(data[i].internalGeneID);
                    tmp.setName(data[i].geneID);
                    tmp._status = true;
                };

                self.updateStatus();
            };

            dataBaseCall.call();
        }
    }};

TranslateToEnsembl.prototype.updateStatus = function(){};


function TranslateToAlternative() {
    this._status = false;
};

TranslateToAlternative.prototype.init = function(geneset,alternativeSource){
	//console.log(geneset);
    if ( geneset instanceof Map) {
        this._status = true;
        
        var self = this;
        self.genesetA = geneset;

        var queryIDSetTHIS = [];
        self.genesetA.forEach(function(value, key) {
                  if(value.alternativeID.get(alternativeSource) == null)
                  	 queryIDSetTHIS.push(value.name);
                },queryIDSetTHIS); 


          	
        if (queryIDSetTHIS.length > 0) {
			document.body.style.cursor='wait';	
            
            var dataBaseCall = new DataBaseCall();
            dataBaseCall.setMethod('internalToAlternative');
            dataBaseCall.setSpecies(self.genesetA.values().next().value.species.id);
            dataBaseCall.setRequest(queryIDSetTHIS + '&alternativeID=' + alternativeSource);

            dataBaseCall.update = function(data) { 
                for (var i in data) {
                	

                    var tmp = global.GENESET.get(parseInt(data[i].internalGeneID));
                    
                    if(data[i].displayID!='null'){
                    	tmp.alternativeID.set(alternativeSource,data[i].displayID);
                    }else{
                    	tmp.alternativeID.set(alternativeSource,"");     	
                    }
                    tmp._status = true;

                };
                self.updateStatus();  
				document.body.style.cursor='default';	         
            };

            dataBaseCall.call();
            
            
        }
       
        
    }};

TranslateToAlternative.prototype.updateStatus = function(){};


/* ****************** ***********************
 *     			   HELPER FUNCTIONS
 ******************** ******************** */

function sumRandomLinks(termset,pathway) {
    if ( pathway instanceof Base) {
        var qeryIDSet = [];
            termset.boundQuery.forEach(function(value, key) {
              qeryIDSet.push(key);
            },qeryIDSet);
        var pathwayGenes = [];
            pathway.genes.forEach(function(value, key) {
              pathwayGenes.push(key);
            },pathwayGenes);
    
        var dataBaseCall = new DataBaseCall();
        dataBaseCall.setMethod('sumRandomLinks');
        dataBaseCall.setSpecies(termset.species.id);
        dataBaseCall.setRequest(qeryIDSet + '&neighbour=' + pathwayGenes);
    
        dataBaseCall.update = function(data) {
            $('#query_status').text(pathway.name);
            $('#pathway_nr').html(parseInt($('#pathway_nr').html()) - 1);
            pathway.setRandomLinks(data);
            pathway.validate();
            termset.validate();
        };
    
        dataBaseCall.call();
    
     } 
};



/* ------------------------------------- */
function sumArray(array) {
	return array.reduce(function(pv, cv) {
		return pv + cv;
	}, 0);
}

/* ------------------------------------- */
//hackish swapping
function swap(x) {
	return x;
};


function FunCoupLink(gene,pathway){
    var relationalGenes = '';
    pathway.genes.forEach(function (i) {relationalGenes = relationalGenes +'+'+  i.name ; },relationalGenes);
    var url = 'http://funcoup.sbc.su.se/search/network.action?' ;
    var query = 'query.geneQuery='+gene ;
    var genomeID = '&query.primaryGenomeID='+global.RESULT.species.fcGenomeID[0] ;
    var cutoff = '&query.confidenceThreshold=0.8' ;
    var depth = '&query.depth=1' ;
    var nodesPerStep = '&query.nodesPerStep=30' ;
    var options = '&query.expansionAlgorithm=group&query.prioritizeNeighbors=true&__checkbox_query.prioritizeNeighbors=true&__checkbox_query.addKnownCouplings=true&query.individualEvidenceOnly=true&__checkbox_query.individualEvidenceOnly=true&query.categoryID=-1&query.constrainEvidence=no&query.restriction=genes';
    var restriction = '&query.relationalGenes='+relationalGenes+'&query.showAdvanced=true';
    
    return url+query+genomeID+cutoff+depth+nodesPerStep+options+restriction ; 
}

function FunCoupLinkGenes(queryGeneList,pathwayGeneList){
    var relationalGenes = '';
    var genes = '';
    queryGeneList.forEach(function (i) {genes = genes +'+'+  i ; },genes);
    pathwayGeneList.forEach(function (i) {relationalGenes = relationalGenes +'+'+  i ; },relationalGenes);
    var url = 'http://funcoup.sbc.su.se/search/network.action?' ;
    var query = 'query.geneQuery='+ genes ;
    var genomeID = '&query.primaryGenomeID='+global.RESULT.species.fcGenomeID[0] ;
    var cutoff = '&query.confidenceThreshold=0.8' ;
    var depth = '&query.depth=1' ;
    var nodesPerStep = '&query.nodesPerStep=30' ;
    var options = '&query.expansionAlgorithm=group&query.prioritizeNeighbors=true&__checkbox_query.prioritizeNeighbors=true&__checkbox_query.addKnownCouplings=true&query.individualEvidenceOnly=true&__checkbox_query.individualEvidenceOnly=true&query.categoryID=-1&query.constrainEvidence=no&query.restriction=genes';
    var restriction = '&query.relationalGenes='+relationalGenes+'&query.showAdvanced=true';
    
    return url+query+genomeID+cutoff+depth+nodesPerStep+options+restriction; 
}

function getDbLink(pathwayName){
	var url = '';
	if($('#termsetSelection').val() == 0){
		url = 'http://www.genome.jp/dbget-bin/www_bget?';
    }else{
		url = 'http://reactome.org/content/detail/';
    }
    return url+pathwayName.toUpperCase(); 
}
